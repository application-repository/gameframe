## **GommeHD.net Developer Application**

#### Test Server: kybu.dev
#### Java Documentation: https://kybu.dev/docs/GameFrame


Sehr geehrtes _GommeHD.net_ Developer Team,
dies hier ist der zweite Teil meiner Bewerbung zum Developer.

Hier findet ihr 3 Projekte, das erste (das hier), um genau zu sein das **GameFrame**,
ist das Herz des Projekts. Das hier kann man sozusagen vergleichen mit einer GameAPI.

Diese API umfasst alle grundlegende Dinge, um ein rundebasiertes oder ein FFA Game zu programmieren.
Beispiele für die Anwendung dieser API findet ihr in BedWars oder QuickShot.

Zusätzlich beinhaltet dieses GameFrame auch das gesamte Modul System dieser API.

## **Kurze Anleitung**

Als erstes verbindet man sich auf den Minecraft-Server "kybu.dev".
Sollte bereits ein Modul geladen sein, welches nicht geladen sein sollte, macht man einfach **"/module drop"**, um das Modul zu entladen.

Sobald man verbunden ist und der Server sich um Ruhezustand befindet, kann man einen Modus auswählen. 
Eine Liste der verfügbaren Modi findet man unter **"/module list"**, aber ich Spoiler euch hier zu schon mal, es gibt nur zwei.
Diese zwei Modi sind einmal **BedWars** und **QuickShot**. Sollte man sich dann für einen Modus entschieden haben, kann
man es entweder im GUI anklicken oder **"/module load (Modus)"** benutzen, um den Modus anschließend zu laden.

Direkt darauf werden alle Online Spieler dem Spiel hinzugefügt, überflüssige Spieler werden rausgeworfen.
Darauf startet dann das ausgewählte Game.

Sollte man keine Lust mehr auf diesen Modus haben, macht man **"/module drop"** um den Modus zu entladen. 
Darauf kann man einfach wieder einen neuen Modus laden.

## **BedWars**

Das BedWars hier ist eine abgespickte Version des BedWars auf dem GommeHD.net Netzwerk. Um genau zu sein fehlen nur ein paar Items.
Auch das Spielprinzip ist das gleiche, die Spieler spawnen auf Inseln, werden durch die Spawner mit Ressourcen durchgefüttert und bekämpfen sich dann.

## **QuickShot**

QuickShot ist im Gegensatz zu BedWars ein eigen konzipierter Modus, nur mit typischen Eigenschaften eines GommeHD.net Spielmodi.
Sobald sich genügend Spieler versammelt haben, startet das Spiel.
Erst einmal geht es in die Vorbereitungsphase, in welcher Spieler ein Schwert, Bogen und ein Pfeil bekommen.
Das Ganze läuft dann im OITC Style ab, alle Spieler sind One-Hit, pro Kill bekommt man 20 Level und einen Pfeil dazu.

Nach 5 Minuten startet dann die Shopping Phase, in der kann man mit den gesammelten Level Equipment für die darauffolgende Kampfphase sammeln.
Diese dauert um die eine Minute an.

Nachdem die Shopping Phase zu ende ist, startet man ins Deathmatch. Hierfür werden alle Teilnehmer in die Deathmatch Arena teleportiert.
Dort müssen sich dann alle im FFA Style bekämpfen, jeder hat 3 Leben, wenn diese bei 0 ankommen ist man raus und hat verloren.

Der letzte überlebende ist der Gewinner.

## **Abschluss**

Ich habe für alles hier insgesamt 4 Wochen gebraucht, das wären dann eine Woche pro Projekt, und die letzte Woche zur Dokumentation und ein paar Bug Fixes.
Anmerken möchte ich, das es zu ein paar Bugs kommen kann, da ich nicht ausgiebig testen konnte. Für diese Bugs (wenn welche da sind) möchte ich mich entschuldigen.

Ich hoffe, ich konnte euch mit diesem Projekt von mir, meiner Entschlossenheit und meinem Engagement überzeugen.

Mit freundlichen Grüßen,
Felix "kybuu" Clay