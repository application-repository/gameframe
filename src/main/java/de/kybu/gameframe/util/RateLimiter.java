package de.kybu.gameframe.util;

import de.kybu.gameframe.GameFrame;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;

import java.io.Closeable;
import java.io.IOException;

/**
 * Custom Rate Limiter Klasse um
 * die maximalen Executions in einer Sekunde zu regulieren
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
@Getter
@Setter
public class RateLimiter implements Closeable {

    private int executions;
    private final int maxExecutions;
    private int scheduler;

    /**
     * Konstruktor der {@link RateLimiter} Klasse
     * @param maxExecutions - die maximalen Executions pro Sekunde
     */
    public RateLimiter(int maxExecutions){
        this.maxExecutions = maxExecutions;
        this.executions = 0;

        this.scheduler = Bukkit.getScheduler().scheduleSyncRepeatingTask(GameFrame.getInstance(), () -> {
            executions = 0;
        }, 20, 20);
    }

    /**
     * Überprüft ob etwas ausgeführt werden kann,
     * oder ob das Rate Limit bereits überschritten ist.
     *
     * Wenn nicht, wird zu den derzeitigen Ausführungen eins dazu gezählt.
     * @return ob die Ausführung erlaubt ist
     */
    public boolean exec(){
        if(executions >= this.maxExecutions)
            return false;

        executions++;
        return true;
    }

    @Override
    public void close() throws IOException {
        Bukkit.getScheduler().cancelTask(scheduler);
    }
}
