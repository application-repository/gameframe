package de.kybu.gameframe.util;

import de.kybu.gameframe.util.message.AbstractMessage;
import de.kybu.gameframe.util.player.UserService;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

import java.util.function.Predicate;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class BukkitUtil {

    public static void clearInventory(@NonNull final Player player){
        final PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setArmorContents(null);
    }

    /**
     * Setzt die Werte des Spielers auf den Standard zurück
     *
     * @param player - das {@link Player} Objekt des Ziels
     * @param resetHealth - ob die Leben auf 20 zurückgesetzt werden sollen
     * @param resetLevel - ob die Level auf 0 zurückgesetzt werden sollen
     */
    public static void resetPlayer(@NonNull final Player player, final boolean resetHealth, final boolean resetLevel){
        if(resetHealth)
            player.setMaxHealth(20);

        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setSaturation(5);
        player.setExhaustion(0);
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
        player.setRemainingAir(300);
        player.setFireTicks(0);
        player.setFallDistance(0);
        player.setFlying(false);
        player.setAllowFlight(false);
        player.setVelocity(new Vector(0, 0, 0));

        if(resetLevel){
            player.setLevel(0);
            player.setExp(0);
        }
    }

    /**
     * Zeigt alle versteckten Spieler wieder an
     * @param player - das {@link Player} Objekt des Ziels
     */
    public static void showAllHiddenPlayers(@NonNull final Player player){
        player.spigot().getHiddenPlayers().forEach(player::showPlayer);
    }

    /**
     * Versteckt allen Spielern, die unter die Bedingung fallen die "Zielperson"
     * @param player - das {@link Player} Objekt des Ziels
     * @param predicate - welche Spieler den Spieler versteckt bekommen sollen
     */
    public static void hidePlayers(@NonNull final Player player, Predicate<Player> predicate){
        for(Player players : Bukkit.getOnlinePlayers()){
            if(predicate.test(players))
                players.hidePlayer(player);
        }
    }

    /**
     * Sendet eine globale {@link AbstractMessage} an alle Online Player
     * @param abstractMessage - die {@link AbstractMessage}
     * @see de.kybu.gameframe.util.player.User#sendMessage(AbstractMessage) 
     */
    public static void broadcastMessage(@NonNull final AbstractMessage abstractMessage){
        UserService.getInstance().getOnlineUsers().forEach(user -> user.sendMessage(abstractMessage));
    }

    /**
     * Sendet eine globale Nachricht an alle Online Player
     * @param message - die Nachricht
     * @see de.kybu.gameframe.util.player.User#sendMessage(String) 
     */
    public static void broadcastMessage(@NonNull final String message){
        UserService.getInstance().getOnlineUsers().forEach(user -> user.sendMessage(message));
    }

    /**
     * Sendet eine globale übersetzte Nachricht an alle Online Spieler
     * @param translationKey - der Translation Key
     * @param replacements - die Replacements
     * @see de.kybu.gameframe.util.player.User#sendTranslatedMessage(String, String...) 
     */
    public static void broadcastTranslatedMessage(@NonNull final String translationKey, String... replacements){
        UserService.getInstance().getOnlineUsers().forEach(user -> user.sendTranslatedMessage(translationKey, replacements));
    }

}
