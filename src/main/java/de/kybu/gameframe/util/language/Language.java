package de.kybu.gameframe.util.language;

import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.cubespace.Yamler.Config.YamlConfig;
import org.bukkit.Location;

import java.io.File;
import java.util.Collections;
import java.util.Map;

/**
 * Konfigurations Klasse
 * <p>
 *     Diese Konfiguration beinhaltet alle
 *     Übersetzungen sowie den languageKey
 *     der jeweiligen Sprache.
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */

@Setter
@Getter
public class Language extends YamlConfig {

    private String languageKey;
    private Map<String, String> translations;

    /**
     * Konstruktor der {@link Language} Klasse
     * <p>
     *     In dieser Klasse wird nur via dem languageKey Paramater
     *     der Standard Parameter gesetzt.
     *
     *     Darauf wird die CONFIG_FILE gesetzt.
     * </p>
     * @param languageKey
     */
    public Language(final String languageKey){
        this.languageKey = languageKey;
        CONFIG_FILE = new File("plugins/GameFrame/language", languageKey + ".yml");
    }

    /**
     * Methode, welche eine Übersetzung für die gesuchte Sprache zurückgibt.
     * <p>
     *     Diese Methode gibt die übersetze Nachricht für
     *     den angegebenen Key zurück.
     * </p>
     * <p>
     *     Hierbei werden alle %s nach der Reihenfolge
     *     wie sie rann kommen, durch den nächsten String
     *     in den Replacements ersetzt
     * </p>
     * @param key, den Key der gesuchten Übersetzung
     * @param replacements, alle Replacements der Übersetzung
     * @return Übersetze Nachricht
     */
    public String getTranslation(String key, String... replacements){
        if(translations.containsKey(key)){
            String translation = translations.get(key);
            translation = translation.replace("&", "§");
            return String.format(translation, replacements);
        }

        this.translations.put(key, "Missing translation :(");
        try {
            this.save();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        return "§cMissing translation for " + key + " at Language " + languageKey;
    }

}
