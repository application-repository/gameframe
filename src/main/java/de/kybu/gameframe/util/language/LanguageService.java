package de.kybu.gameframe.util.language;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.util.language.global.PlayerLanguageConfig;
import lombok.Getter;
import lombok.NonNull;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * Diese Klasse soll als Utility Klasse
 * für das Multi Language System dienen.
 *
 * Hiermit können Spieler Sprachen gesetzt und geladen werden.
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */

@Getter
public class LanguageService {

    private static LanguageService instance;

    private final Map<String, Language> languages;

    /**
     * @return Instanz der {@link LanguageService} Klasse
     */
    public static LanguageService getInstance() {
        return (instance != null ? instance : (instance = new LanguageService()));
    }

    /**
     * Konstruktor der {@link LanguageService} Klasse
     */
    public LanguageService(){
        this.languages = new HashMap<>();
        this.cacheLanguages();
    }

    /**
     * Cached alle verfügbaren Sprachen
     */
    private void cacheLanguages(){
        availableLanguages().forEach(s -> {
            Language language = new Language(s);
            try {
                language.init();
            } catch (InvalidConfigurationException e) {
                e.printStackTrace();
            }
            this.languages.put(s, language);
        });
    }

    /**
     * @return Eine Liste, mit allen verfügbaren Sprachen
     */
    private List<String> availableLanguages(){
        List<String> list = new ArrayList<>();

        for (File file : new File("plugins/GameFrame/language/").listFiles()) {
            if(file.getName().equals("users.yml"))
                continue;

            if(file.getName().endsWith(".yml"))
                list.add(file.getName().replace(".yml", ""));
        }

        return list;
    }

    /**
     * Ändert die vom Spieler gesetzte Sprache zu einer anderen.
     *
     * @param player - der Spieler
     * @param newLanguage - die neue Sprache
     */
    public void updateLanguage(final Player player, final String newLanguage){
        PlayerLanguageConfig config = PlayerLanguageConfig.getPlayerLanguageConfig();
        config.update(player.getUniqueId().toString(), newLanguage);
    }

    /**
     * Gibt die derzetige vom Spieler ausgewählte Sprache zurück
     *
     * @param player - der Spieler
     * @return Ein Future Objekt, welches hoffentlich die Language beinhaltet
     */
    public Future<Language> getPlayerLanguage(@NonNull final Player player){
        return GameFrame.getInstance().getExecutorService().submit(() -> {
            String userLanguage = PlayerLanguageConfig.getPlayerLanguageConfig().getLanguage(player.getUniqueId());
            if(!languages.containsKey(userLanguage)){
                Language language = new Language(userLanguage);
                try {
                    language.init();
                } catch (InvalidConfigurationException e) {
                    e.printStackTrace();
                }
                this.languages.put(userLanguage, language);
            }


            return this.languages.get(userLanguage);
        });
    }

    public Map<String, Language> getLanguages() {
        return languages;
    }
}
