package de.kybu.gameframe.util.language.global;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.cubespace.Yamler.Config.YamlConfig;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Konfigurations Klasse
 * <p>
 *     Diese Konfiguration beinhaltet die eingestellte
 *     Sprache der Spieler
 * </p>
 *
 * @author Felix Clay (kybuu)
 */
@Getter
@Setter
public class PlayerLanguageConfig extends YamlConfig {

    private static PlayerLanguageConfig playerLanguageConfig;

    private Map<String, String> userLanguages;

    /**
     * Konstruktor der {@link PlayerLanguageConfig} Klasse
     * <p>
     *     In diesem Konstruktor wird standardmäßig
     *     die HashMap userLanguages zu einer neuen HashMap gesetzt.
     *     Falls keine Konfiguration geladen werden konnten wird dann
     *     auf der Basis dieser HashMap eine neue erstellt.
     * </p>
     */
    public PlayerLanguageConfig(){
        this.userLanguages = new HashMap<>();
        CONFIG_FILE = new File("plugins/GameFrame/language", "users.yml");
        try {
            this.init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gibt die Sprache des Spielers zurück
     * <p>
     *     Diese Method gibt die eingestellte Sprache des Spielers zurück.
     *     Hierbei wird die userLanguage HashMap verwendet, und dort gecached.
     * </p>
     * @param uuid, UUID des Spielers
     * @return Sprache des Spielers
     */
    public String getLanguage(@NonNull final UUID uuid){
        synchronized (this.userLanguages){
            if(!this.userLanguages.containsKey(uuid.toString())){
                this.userLanguages.put(uuid.toString(), "Deutsch");
                try {
                    this.save();
                } catch (InvalidConfigurationException e) {
                    e.printStackTrace();
                }
            }
        }

        return this.userLanguages.get(uuid.toString());
    }

    /**
     * Gibt eine Instanz der Klasse {@link PlayerLanguageConfig} zurück.
     * @return Instanz der Klasse {@link PlayerLanguageConfig}
     */
    public static PlayerLanguageConfig getPlayerLanguageConfig() {
        if(playerLanguageConfig == null){
            playerLanguageConfig = new PlayerLanguageConfig();
        }

        return playerLanguageConfig;
    }

    /**
     * Updatet die Sprache des Spielers
     * <p>
     *     Diese Methode ändert die eingestellte Sprache des
     *     Spielers und speichert anschließend die
     *     Konfiguration wieder ab, damit diese Einstellung nicht
     *     verloren geht.
     * </p>
     * @param uuid, UUID des Spielers
     * @param newLanguage, neue Sprache des Spielers
     */
    public void update(final String uuid, final String newLanguage){
        if(this.userLanguages.containsKey(uuid))
            this.userLanguages.remove(uuid);

        this.userLanguages.put(uuid, newLanguage);
        try {
            this.save();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
}
