package de.kybu.gameframe.util;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Um zwei kämpfende Spieler zu loggen
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class CombatLog {

    private static CombatLog instance;

    /**
     * @return Instanz der {@link CombatLog} Klasse
     */
    public static CombatLog getInstance() {
        return (instance != null ? instance : (instance = new CombatLog()));
    }

    private final Map<Player, Player> map;

    /**
     * Konstruktor der {@link CombatLog} Klasse
     */
    public CombatLog(){
        this.map = new HashMap<>();
    }

    /**
     * Setzt einen Spieler in den "Kampfstatus"
     * @param enemy - der angegriffene Spieler
     * @param damager - der angreifende Spieler
     */
    public void addFightingPlayers(final Player enemy, final Player damager){
        this.map.put(enemy, damager);
    }

    /**
     * Überprüft ob eine Spieler im Kampf ist
     * @param player - der zu überprüfende Spieler
     * @return
     */
    public boolean isInCombat(final Player player){
        return this.map.containsKey(player);
    }

    /**
     * Entfernt einen Spieler aus dem Kampfstatus
     * @param player - der Spieler
     */
    public void removePlayer(final Player player){
        this.map.remove(player);
    }

    public Map<Player, Player> getMap() {
        return map;
    }
}
