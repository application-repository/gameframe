package de.kybu.gameframe.util;

import lombok.*;
import net.cubespace.Yamler.Config.YamlConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Wrapper Klasse
 * <p>
 *     Diese Klasse dient ausschließlich als
 *     Wrapper oder Konverter Klasse für das
 *     {@link Location} Objekt.
 * </p>
 * <p>
 *     Diese Klasse kann via dem
 *     {@link de.kybu.gameframe.configurations.ConfigurationService} als Json serializiert oder
 *     via der {@link YamlConfig} als Yaml serializiert werden
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@AllArgsConstructor
public class XYZW extends YamlConfig {

    private String world;

    private float x;
    private float y;
    private float z;

    private float yaw;
    private float pitch;

    /**
     * Konstruktor der {@link XYZW} Klasse
     * <p>
     *     Diese Konstruktor setzt der Klasse
     *     ein paar Standard Werte, sollte diese
     *     nicht serializiert werden können um damit
     *     einer {@link NullPointerException} zu entgehen
     * </p>
     */
    public XYZW(){
        this.world = "";
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.yaw = 0;
        this.pitch = 0;
    }

    /**
     * Wandelt das Objekt zu einer {@link Location} um
     * @return Location mit den gesetzten werten
     */
    public Location toLocation(){
        Location location = new Location(Bukkit.getWorld(this.world), this.x, this.y, this.z);
        location.setYaw(this.yaw);
        location.setPitch(this.pitch);

        return location;
    }
}
