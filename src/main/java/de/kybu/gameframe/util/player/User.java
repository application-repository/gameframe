package de.kybu.gameframe.util.player;

import de.kybu.gameframe.util.RateLimiter;
import de.kybu.gameframe.util.language.Language;
import de.kybu.gameframe.util.language.LanguageService;
import de.kybu.gameframe.util.message.AbstractMessage;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.util.CraftChatMessage;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * User Klasse
 *
 * @author Felix Clay (kybuu)
 * @since 2/5/2021
 */

@Getter
@Setter
public class User {

    private String userName;
    private UUID uuid;
    private Future<Language> playerLanguage;
    private PlayerConnection playerConnection;
    private Map<Object, Object> playerMetaData;
    private RateLimiter rateLimiter;

    /**
     * Konsturktor der {@link User} Klasse
     * @param player - der Spieler, für welchen ein User Objekt erzeugt werden soll
     */
    public User(@NonNull final Player player){
        this.userName = player.getName();
        this.uuid = player.getUniqueId();
        this.playerMetaData = new ConcurrentHashMap<>();
        this.playerLanguage = LanguageService.getInstance().getPlayerLanguage(player);
        this.playerConnection = ((CraftPlayer) player).getHandle().playerConnection;
        this.rateLimiter = new RateLimiter(1);
    }

    /**
     * Sendet eine {@link AbstractMessage} an den Spieler
     * @param message
     */
    public void sendMessage(@NonNull final AbstractMessage message){
        message.sendMessage(this.playerConnection);
    }

    /**
     * Sendet eine String Message and den Spieler
     * @param message
     */
    public void sendMessage(@NonNull final String message){
        for(IChatBaseComponent chatBaseComponent : CraftChatMessage.fromString(message))
            playerConnection.sendPacket(new PacketPlayOutChat(chatBaseComponent));
    }

    /**
     * Sendet eine übersetzte Nachricht an den Spieler
     * @param key - Key der übersetzte Nachricht
     * @param replacements - Replacements der Nachricht
     */
    public void sendTranslatedMessage(final String key, final String... replacements){
         sendMessage(this.getPlayerLanguage().getTranslation(key, replacements));
    }

    /**
     * Fügt einen internen Wert zu einem Spieler hinzu
     * @param key - Key
     * @param value - Value
     */
    public void addMetaData(final Object key, final Object value){
        this.playerMetaData.put(key, value);
    }

    /**
     * Nimmt einen Wert aus dem Spieler Objekt,
     * wenn es noch nicht vorhanden ist und autoInsert true ist,
     * wird der Value mit dem Key gespeichert
     *
     * @param key - Key
     * @param value - Value
     * @param autoInsert - Einfügen, wenn nicht vorhanden
     * @return - Eingetragener Value
     */
    public Object getOrElse(final Object key, final Object value, final boolean autoInsert){
        if(!this.playerMetaData.containsKey(key)){
            if(autoInsert)
                this.playerMetaData.put(key, value);

            return value;
        }
        return this.playerMetaData.get(key);
    }

    /**
     * Alle interne Werte des Spielers löschen
     */
    public void clearMetaData(){
        this.playerMetaData.clear();
    }

    /**
     * Überprüft ob ein Spieler einen Wert hat
     * @param key - Key
     * @return ob der Spieler den Wert hat
     */
    public boolean hasMetaData(final Object key){
        return this.playerMetaData.containsKey(key);
    }

    /**
     * Ob ein Spieler etwas ausführen kann
     * @return Ob die Auführung erlaubt ist
     * @see RateLimiter#exec()
     */
    public boolean canExecute(){
        return this.rateLimiter.exec();
    }

    /**
     * Updatet die Sprache des Spielers
     * @param player - der Spieler
     */
    public void updateLanguage(final Player player){
        this.playerLanguage = LanguageService.getInstance().getPlayerLanguage(player);
    }

    /**
     * Gibt die festgelegte Sprache des Spielers zurück
     * @return {@link Language} des Spielers
     */
    public Language getPlayerLanguage(){
        try {
            return this.playerLanguage.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}
