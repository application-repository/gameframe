package de.kybu.gameframe.util.player;

import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 2/5/2021
 */
public class UserService {

    private static UserService instance;

    private final Map<UUID, User> internalUserCache;

    /**
     * @return Instanz des {@link UserService} Objekts
     */
    public static UserService getInstance() {
        return (instance != null ? instance : (instance = new UserService()));
    }

    /**
     * Konstruktor der {@link UserService} Klasse
     */
    public UserService(){
        this.internalUserCache = new ConcurrentHashMap<>();
    }

    /**
     * Gibt einen gespeicherten Spieler zurück
     * @param uuid - {@link UUID} des Spielers
     * @return {@link User} Objekt des Spielers
     */
    public User getUser(final UUID uuid){
        return this.internalUserCache.get(uuid);
    }

    /**
     * Registriert einen Spieler
     * @param player - das {@link Player} Objekt des Spielers
     */
    public void registerUser(final Player player){
        this.internalUserCache.put(player.getUniqueId(), new User(player));
    }

    /**
     * Entfernt einen registrierten Spieler
     * @param uuid - die {@link UUID} des Spielers
     */
    public void dropUser(final UUID uuid){
        this.internalUserCache.remove(uuid);
    }

    /**
     * Gibt alle Online Spieler, welche ein User Objekt besitzen zurück
     * @return User Collection mit Online Spielern
     */
    public Collection<User> getOnlineUsers(){
        return internalUserCache.values();
    }
}
