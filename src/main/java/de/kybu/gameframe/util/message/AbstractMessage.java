package de.kybu.gameframe.util.message;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.PlayerConnection;

/**
 * Abstrakte Klasse, welche als Masterobjekt
 * für eine Spielernachricht dient
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 * @see ActionBarMessage
 * @see ClickableMessage
 */

@Getter
@Setter
public abstract class AbstractMessage {

    private String message;

    public AbstractMessage(final String message){
        this.message = message;
    }

    public abstract void sendMessage(@NonNull final PlayerConnection playerConnection);

}
