package de.kybu.gameframe.util.message;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PlayerConnection;

/**
 * Sub-Klasse von {@link AbstractMessage}, welche dazu
 * dient, einem Spieler eine Action Bar Nachricht zu
 * senden.
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class ActionBarMessage extends AbstractMessage{

    private final PacketPlayOutChat packetPlayOutChat;

    /**
     * Konstruktor der {@link ActionBarMessage} Klasse
     * @param message - Nachricht
     */
    public ActionBarMessage(String message) {
        super(message);
        this.packetPlayOutChat = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(String.format("{\"text\":\"%s\"}", message)), (byte) 2);
    }

    /**
     * Sendet die gesetzte Nachricht per ActionBar an die {@link PlayerConnection}
     * @param playerConnection - die {@link PlayerConnection} vom Spieler
     */
    @Override
    public void sendMessage(final PlayerConnection playerConnection){
        playerConnection.sendPacket(packetPlayOutChat);
    }
}
