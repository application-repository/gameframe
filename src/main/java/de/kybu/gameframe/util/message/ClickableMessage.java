package de.kybu.gameframe.util.message;

import lombok.NonNull;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PlayerConnection;

/**
 * Sub-Klasse von {@link AbstractMessage}, welche dazu
 * dient, einem Spieler klickbare Nachrichten
 * zu senden
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class ClickableMessage extends AbstractMessage {

    private final PacketPlayOutChat packetPlayOutChat;
    private final TextComponent textComponent;

    /**
     * Konstruktor der {@link ClickableMessage} Klasse
     * @param message - Nachricht
     */
    public ClickableMessage(@NonNull final String message) {
        super(message);
        this.textComponent = new TextComponent(message);
        this.packetPlayOutChat = new PacketPlayOutChat();
        this.packetPlayOutChat.components = new BaseComponent[]{textComponent};
    }

    /**
     * Fügt ein {@link ClickEvent} zu dem {@link TextComponent} hinzu
     * @param event - das Klick Event
     * @return das derzeitige Objekt
     */
    public ClickableMessage addClickEvent(@NonNull final ClickEvent event){
        this.textComponent.setClickEvent(event);
        return this;
    }

    /**
     * Fügt ein {@link HoverEvent} zu dem {@link TextComponent} hinzu
     * @param event - das Hover Event
     * @return das derzeitige Objekt
     */
    public ClickableMessage addHoverEvent(@NonNull final HoverEvent event){
        this.textComponent.setHoverEvent(event);
        return this;
    }

    /**
     * Sendet die gesetzte Nachricht per Chat an die {@link PlayerConnection}
     * @param playerConnection - die {@link PlayerConnection} vom Spieler
     */
    @Override
    public void sendMessage(@NonNull final PlayerConnection playerConnection){
        playerConnection.sendPacket(packetPlayOutChat);
    }


}
