package de.kybu.gameframe.item;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagString;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.List;

import java.util.ArrayList;

public class Item {

    private ItemStack item;
    private final List<String> lore = new ArrayList<>();
    private final ItemMeta meta;
    private net.minecraft.server.v1_8_R3.ItemStack nmsItemStack;

    public Item(Material mat, short subid, int amount) {
        item = new ItemStack(mat, amount, subid);
        meta = item.getItemMeta();
        this.nmsItemStack = CraftItemStack.asNMSCopy(this.item);
    }

    public Item(ItemStack item) {
        this.item = item;
        this.meta = item.getItemMeta();
        this.nmsItemStack = CraftItemStack.asNMSCopy(this.item);
    }

    public Item(Material mat, short subid) {
        item = new ItemStack(mat, 1, subid);
        meta = item.getItemMeta();
        this.nmsItemStack = CraftItemStack.asNMSCopy(this.item);
    }

    public Item(Material mat, int amount) {
        item = new ItemStack(mat, amount, (short) 0);
        meta = item.getItemMeta();
        this.nmsItemStack = CraftItemStack.asNMSCopy(this.item);
    }

    public Item(Material mat) {
        item = new ItemStack(mat, 1, (short) 0);
        meta = item.getItemMeta();
        this.nmsItemStack = CraftItemStack.asNMSCopy(this.item);
    }

    public Item setAmount(int value) {
        item.setAmount(value);
        return this;
    }

    public Item setNoName() {
        meta.setDisplayName(" ");
        return this;
    }

    public Item setGlow() {
        meta.addEnchant(Enchantment.DURABILITY, 1, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return this;
    }

    public Item setData(short data) {
        item.setDurability(data);
        return this;
    }

    public Item addLoreLine(String line) {
        lore.add(line);
        return this;
    }

    public Item addLoreArray(String[] lines) {
        for (int x = 0; x < lines.length; x++) {
            lore.add(lines[x]);
        }
        return this;
    }

    public Item addLoreAll(List<String> lines) {
        lore.addAll(lines);
        return this;
    }

    public Item setDisplayName(String name) {
        meta.setDisplayName(name);
        return this;
    }

    public Item setSkullOwner(String owner) {
        ((SkullMeta) meta).setOwner(owner);
        return this;
    }

    public Item setColor(Color c) {
        ((LeatherArmorMeta) meta).setColor(c);
        return this;
    }

    public Item setBannerColor(DyeColor c) {
        ((BannerMeta) meta).setBaseColor(c);
        return this;
    }

    public Item setUnbreakable(boolean value) {
        meta.spigot().setUnbreakable(value);
        return this;
    }

    public Item addEnchantment(Enchantment ench, int lvl) {
        meta.addEnchant(ench, lvl, true);
        return this;
    }

    public Item addItemFlag(ItemFlag flag) {
        meta.addItemFlags(flag);
        return this;
    }

    public Item addLeatherColor(Color color) {
        ((LeatherArmorMeta) meta).setColor(color);
        return this;
    }

    public ItemStack build() {
        if (!lore.isEmpty()) {
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        this.nmsItemStack = CraftItemStack.asNMSCopy(this.item);
        NBTTagCompound nbtTagCompound = (nmsItemStack.hasTag() ? nmsItemStack.getTag() : new NBTTagCompound());
        return item;
    }

    public static ItemStack addTag(final ItemStack itemStack, final String key, final String value){
        net.minecraft.server.v1_8_R3.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = nmsItemStack.getTag() != null ? nmsItemStack.getTag() : new NBTTagCompound();
        nbtTagCompound.set(key, new NBTTagString(value));
        nmsItemStack.setTag(nbtTagCompound);
        return CraftItemStack.asBukkitCopy(nmsItemStack);
    }
    
    public static String readTag(final ItemStack itemStack, final String key){
        net.minecraft.server.v1_8_R3.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = nmsItemStack.getTag() != null ? nmsItemStack.getTag() : new NBTTagCompound();

        if(nbtTagCompound.hasKey(key))
            return nbtTagCompound.getString(key);

        return "FAIL";
    }

}
