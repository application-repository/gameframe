package de.kybu.gameframe.item;

import de.kybu.gameframe.GameFrame;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */


@Setter
@Getter
public abstract class ClickableItem extends Item implements Listener {

    public ClickableItem(@NonNull final Material material, @NonNull final int amount, final short data){
        super(material, data, amount);
        Bukkit.getPluginManager().registerEvents(this, GameFrame.getInstance());
    }

    public abstract void handle(final Player player);

    @EventHandler
    public void onInteract(final PlayerInteractEvent event){
        if(event.getItem() == null)
            return;

        if(!event.getItem().equals(this.build()))
            return;

        if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
            handle(event.getPlayer());
    }

}
