package de.kybu.gameframe.item.items;

import de.kybu.gameframe.game.inventory.inventories.TeamSelectionInventory;
import de.kybu.gameframe.item.ClickableItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class TeamSelectionItem extends ClickableItem {

    private static TeamSelectionItem instance;

    public TeamSelectionItem() {
        super(Material.BED, 1, (short) 0);
        this.setDisplayName("§eTeamauswahl §7(Rechtsklick)");
    }

    @Override
    public void handle(Player player) {
        player.openInventory(TeamSelectionInventory.getInstance().getInventory());
    }

    public static TeamSelectionItem getInstance() {
        return (instance != null ? instance : (instance = new TeamSelectionItem()));
    }
}
