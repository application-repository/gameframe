package de.kybu.gameframe.item.items;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.inventories.AchievementsInventory;
import de.kybu.gameframe.item.ClickableItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 14.02.2021
 */
public class AchievementsItem extends ClickableItem {

    private static AchievementsItem instance;
    private final Map<UUID, AchievementsInventory> achievementsInventoryMap;

    public AchievementsItem() {
        super(Material.NETHER_STAR, 1, (short) 0);
        this.setDisplayName("§aErfolge §7(Rechtsklick)");
        this.achievementsInventoryMap = new ConcurrentHashMap<>();
    }

    @Override
    public void handle(Player player) {
        Futures.addCallback(open(player), new FutureCallback<Inventory>() {
            @Override
            public void onSuccess(@Nullable Inventory inventory) {
                player.openInventory(inventory);
            }

            @Override
            public void onFailure(Throwable throwable) {
                player.sendMessage("§cFehler beim Laden des Erfolge Inventares!");
            }
        });
    }

    public static AchievementsItem getInstance() {
        return (instance != null ? instance : (instance = new AchievementsItem()));
    }

    public ListenableFuture<Inventory> open(final Player player){
        return GameFrame.getInstance().getExecutorService().submit(() -> {

            if(!achievementsInventoryMap.containsKey(player.getUniqueId()))
                achievementsInventoryMap.put(player.getUniqueId(), new AchievementsInventory(player.getUniqueId()));

            return achievementsInventoryMap.get(player.getUniqueId()).getInventory();
        });
    }

    public Map<UUID, AchievementsInventory> getAchievementsInventoryMap() {
        return achievementsInventoryMap;
    }
}
