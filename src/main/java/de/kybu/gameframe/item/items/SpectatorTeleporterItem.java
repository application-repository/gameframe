package de.kybu.gameframe.item.items;

import de.kybu.gameframe.game.inventory.inventories.SpectatorInventory;
import de.kybu.gameframe.item.ClickableItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class SpectatorTeleporterItem extends ClickableItem {

    private static SpectatorTeleporterItem instance;

    public SpectatorTeleporterItem() {
        super(Material.COMPASS, 1, (short) 0);
        setDisplayName("§eTeleporter");
    }

    @Override
    public void handle(Player player) {
        player.openInventory(SpectatorInventory.getInstance().getInventory());
    }

    public static SpectatorTeleporterItem getInstance() {
        return (instance != null ? instance : (instance = new SpectatorTeleporterItem()));
    }
}
