package de.kybu.gameframe.events;

import de.kybu.gameframe.map.Map;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Event Klasse
 * <p>
 *     Diese Klasse ist ein Event.
 *     Dieses wird getriggered, wenn eine Map via
 *     {@link de.kybu.gameframe.map.MapService#loadMap(String)} geladen wird
 * </p>
 * @author Felix Clay (kybuu)
 */
public class GameFrameMapLoadEvent extends Event {

    public static HandlerList HANDLERS = new HandlerList();

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public Map map;

    public GameFrameMapLoadEvent(Map map){
        this.map = map;
    }

    public Map getMap() {
        return map;
    }
}