package de.kybu.gameframe;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import de.kybu.gameframe.modules.DependenciesInjector;
import de.kybu.gameframe.modules.ModuleService;
import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Main Klasse des GameFrames
 * <p>
 *     Dies hier ist die Main Klasse (Hauptklasse) des GameFrames.
 *     In dieser Klasse läuft die gesamte Start und Stop Logik des GameFrames ab.
 *     Eben so dient sie als Utility Klasse, welche wichtige Sachen wie die
 *     {@link GameFrameConfiguration} oder den {@link ExecutorService} liefert.
 * </p>
 *
 * @author Felix Clay (kybuu)
 */
@Getter
@Setter
public class GameFrame extends JavaPlugin {

    private static GameFrame instance;
    private GameFrameConfiguration gameFrameConfiguration;
    private ListeningExecutorService executorService;

    /**
     * Load Logik des GameFrames
     * <p>
     *     Diese Method wird ausgeführt, wenn der Server die Jar Datei lädt.
     *     Hier werden dann alle gebrauchten Dependencies geladen und ein ThreadPool initialisiert.
     * </p>
     * @author Felix Clay (kybuu)
     */
    @Override
    public void onLoad() {
        instance = this;
        this.executorService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));
        DependenciesInjector.getInstance().injectAll();
    }

    /**
     * Startup Logik des GameFrames
     * <p>
     *     Diese Method wird ausgeführt, wenn der Server das Plugin "startet".
     *     Dies kann allerdings erst passieren, sobald das Plugin geladen wurde.
     * </p>
     *
     * @author Felix Clay (kybuu)
     */
    @Override
    public void onEnable() {
        GameFrameConfiguration gameFrameConfiguration = new GameFrameConfiguration();
        try {
            gameFrameConfiguration.init();
            this.gameFrameConfiguration = gameFrameConfiguration;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        ModuleService.getInstance().registerAllCommandsInPackage("de.kybu.gameframe.commands", this.getClassLoader());
        ModuleService.getInstance().registerAllListenerInPackage("de.kybu.gameframe.listener", this, this.getClassLoader());
    }

    /**
     * Stop Logik des Gameframes
     * <p>
     *     Diese Method wird ausgeführt, wenn das Plugin deaktiviert wird.
     * </p>
     * @author Felix Clay (kybuu)
     */
    @Override
    public void onDisable() {}

    /**
     * Gibt eine Instanz der Klasse {@link GameFrame} zurück.
     * @return Instanz der Klasse {@link GameFrame}
     */
    public static GameFrame getInstance() {
        return instance;
    }
}
