package de.kybu.gameframe.modules.config;

import de.kybu.gameframe.modules.misc.GameSize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.YamlConfig;

import java.util.Arrays;
import java.util.List;

/**
 * Konfigurations Klasse
 * <p>
 *     Diese Konfiguration beinhaltet alle
 *     nötigen Informationen eines Moduls.
 * </p>
 *
 * @author Felix Clay (kybuu)
 */
@Getter
@Setter
@AllArgsConstructor
public class ModuleConfiguration extends YamlConfig {

    private boolean isTurnBased;
    private List<GameSize> allowedGameSize;
    private String moduleColor;
    private String modulePrefix;

    public ModuleConfiguration(){
        this.isTurnBased = false;
        this.allowedGameSize = Arrays.asList(
                new GameSize(4, 2, 8),
                new GameSize(8, 1, 8)
        );
        this.moduleColor = "§6";
        this.modulePrefix = "§f[§6Module§f]";
    }
}
