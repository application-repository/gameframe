package de.kybu.gameframe.modules.misc;

import de.kybu.gameframe.game.Game;
import de.kybu.gameframe.item.Item;
import de.kybu.gameframe.modules.config.ModuleConfiguration;
import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.io.File;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 */
@Setter
@Getter
public class Module {

    private final String moduleName;
    private final String jarName;
    private ModuleConfiguration moduleConfiguration;
    private ItemStack itemStack;
    private Game game;
    private Plugin plugin;

    /**
     * Konstruktor der {@link Module} Klasse
     * @param moduleName - der Name des Modules
     * @param jarName - der Name der Jar-Datei
     */
    public Module(String moduleName, String jarName){
        this.moduleName = moduleName;
        this.jarName = jarName;
        this.loadModuleConfiguration();
        this.itemStack = new Item(Material.BOOK).setDisplayName("§8» §e" + moduleName).build();
    }

    /**
     * Lädt die Modul Konfiguration
     */
    private void loadModuleConfiguration(){
        ModuleConfiguration moduleConfiguration = new ModuleConfiguration();
        try {
            moduleConfiguration.init(new File(String.format("plugins/GameFrame/modules/%s/module.yml", this.moduleName)));
            this.moduleConfiguration = moduleConfiguration;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
}
