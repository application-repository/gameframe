package de.kybu.gameframe.modules.misc;

import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.YamlConfig;

/**
 * Game Size
 *
 * Diese Klasse beinhaltet Informationen
 * über eine Mapgröße
 *
 * @author Felix Clay (kybuu)
 */
@Getter
@Setter
public class GameSize extends YamlConfig {

    private int teamCount;
    private int playersPerTeam;
    private int allowedPlayers;

    /**
     * Konstruktor der {@link GameSize} Klasse
     * @param teamCount - erlaubte Anzahl der maximalen Teams
     * @param playersPerTeam - maximale Spieler pro Team
     * @param allowedPlayers - global maximal erlaubte Spieler
     */
    public GameSize(int teamCount, int playersPerTeam, int allowedPlayers){
        this.teamCount = teamCount;
        this.playersPerTeam = playersPerTeam;
        this.allowedPlayers = allowedPlayers;
    }

    public GameSize(){}
}
