package de.kybu.gameframe.modules;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * Dependency Injector
 *
 * Diese Klasse lädt alle nötigen
 * Dependencies aus dem "dependencies" Ordner
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class DependenciesInjector {

    private static DependenciesInjector instance;
    private final List<File> dependencies;

    public static DependenciesInjector getInstance() {
        return (instance != null ? instance : (instance = new DependenciesInjector()));
    }

    /**
     * Konstruktor der {@link DependenciesInjector} Klasse
     */
    public DependenciesInjector(){
        this.dependencies = new ArrayList<>();
        this.cacheDependencies();
    }

    /**
     * Lädt alle Jar-Dateien aus dem "dependencies" Ordner
     * und speichert sie in der "dependencies" Liste.
     */
    public void cacheDependencies(){
        File parent = new File("dependencies");
        if(!parent.exists())
            parent.mkdir();

        for (File file : parent.listFiles((dir, name) -> name.endsWith(".jar"))) {
            dependencies.add(file);
        }
    }

    /**
     * Injected alle geladenen Dependencies.
     */
    public void injectAll(){
        for (File file : this.dependencies) {
            try {
                addJAR(file);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    private void addJAR(final File jarFile) throws MalformedURLException {
        final URL url = jarFile.toURI().toURL();

        try {
            final Method addMethod = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            addMethod.setAccessible(true);
            addMethod.invoke(ClassLoader.getSystemClassLoader(), url);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
