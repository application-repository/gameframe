package de.kybu.gameframe.modules;

import com.google.common.reflect.ClassPath;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.commands.Command;
import de.kybu.gameframe.commands.CommandLoader;
import de.kybu.gameframe.game.Game;
import de.kybu.gameframe.game.inventory.inventories.SpectatorInventory;
import de.kybu.gameframe.game.inventory.inventories.TeamSelectionInventory;
import de.kybu.gameframe.game.spawner.Spawner;
import de.kybu.gameframe.map.MapService;
import de.kybu.gameframe.modules.misc.Module;
import de.kybu.gameframe.teams.TeamService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.language.Language;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class ModuleService {

    private static ModuleService instance;

    private Map<String, Module> modules;
    private Module activatedModule;
    private TextComponent defaultFooter;

    public static ModuleService getInstance() {
        return (instance != null ? instance : (instance = new ModuleService()));
    }

    /**
     * Konstruktor der {@link ModuleService} Klasse
     */
    public ModuleService(){
        this.modules = new HashMap<>();
        this.cacheModules();
        updateMotd();
    }

    /**
     * Lädt alle verfügbaren Module aus dem "modules" Ordner,
     * erstellt eine Instanz der {@link Module} Klasse für das Modul
     * und fügt diese zu der modules Liste hinzu
     */
    private void cacheModules(){
        File directory = new File("plugins/GameFrame/modules");
        if(!directory.exists())
            directory.mkdirs();

        Arrays.stream(directory.listFiles())
                .filter(File::isFile)
                .filter(file -> file.getName().endsWith(".jar"))
                .forEach(file -> {
                    Module module = new Module(file.getName().split("\\.")[0], file.getAbsolutePath());
                    this.modules.put(file.getName().split("\\.")[0], module);
                });
    }

    /**
     * Deaktiviert das derzeitge Modul
     */
    public void deactivate(){
        if(this.activatedModule != null){
            Game currentGame = this.activatedModule.getGame();
            MapService.getInstance().unloadMap(currentGame.getMap().getMapName());

            Bukkit.getPluginManager().disablePlugin(this.activatedModule.getPlugin());

            if(currentGame.getCurrentCountdown() != null)
                currentGame.getCurrentCountdown().stop();

            TeamService.getInstance().reset();
            Spawner.deleteAll();
            SpectatorInventory.getInstance().getInventory().clear();
            TeamSelectionInventory.getInstance().getInventory().clear();

            this.activatedModule = null;
            BukkitUtil.broadcastTranslatedMessage("module-unloaded", ModuleService.getInstance().getPrefix());
            Bukkit.getOnlinePlayers().forEach(this::setModuleTablist);

            Location location = GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation();

            Bukkit.getOnlinePlayers().forEach(player -> {
                player.teleport(location);
                player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
                BukkitUtil.showAllHiddenPlayers(player);
                BukkitUtil.clearInventory(player);
                BukkitUtil.resetPlayer(player, true, true);
            });

            System.gc();
            updateMotd();
        }
    }

    /**
     * Aktiviert das angegebenen Modul
     * @param moduleName - der Name des zu ladenen Moduls
     */
    public void activateModule(String moduleName){
        Module module = this.getModule(moduleName);
        this.activatedModule = module;

        try {

            if(module.getPlugin() == null)
                module.setPlugin(Bukkit.getPluginManager().loadPlugin(new File(module.getJarName())));

            BukkitUtil.broadcastTranslatedMessage("module-changed", getPrefix(), module.getModuleName());

            Bukkit.getPluginManager().enablePlugin(module.getPlugin());
            Bukkit.getOnlinePlayers().forEach(this::setModuleTablist);
            updateMotd();
        } catch (InvalidPluginException | InvalidDescriptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Registriert alle Befehle in einem Package
     *
     * @param packagePath - der Package Path
     * @param classLoader - der {@link ClassLoader}
     */
    public void registerAllCommandsInPackage(final String packagePath, ClassLoader classLoader){
        try{
            for (ClassPath.ClassInfo classInfo : ClassPath.from(classLoader).getTopLevelClassesRecursive(packagePath)) {
                Class<?> clazz = classInfo.load();
                if(clazz.isAnnotationPresent(Command.class)){
                    CommandLoader.registerCommand(clazz);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * Registriert alle Listener in einem Package
     *
     * @param packagePath - der Package Path
     * @param mainClass - die {@link JavaPlugin} Instanz
     * @param classLoader - der {@link ClassLoader}
     */
    public void registerAllListenerInPackage(final String packagePath, JavaPlugin mainClass, ClassLoader classLoader){
        try {
            for(ClassPath.ClassInfo info : ClassPath.from(classLoader).getTopLevelClassesRecursive(packagePath)){
                Class<?> clazz = info.load();
                if(Listener.class.isAssignableFrom(clazz)){
                    Listener listener = (Listener) clazz.newInstance();
                    Bukkit.getPluginManager().registerEvents(listener, mainClass);
                }

            }
        } catch (IOException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gibt ein ein {@link Module} mit dem
     * angegebenen Namen zurück
     *
     * @param module - der Name des Moduls
     * @return das {@link Module} mit dem gesuchten Namen
     */
    public Module getModule(String module){
        return this.modules.get(module);
    }

    /**
     * Gibt den derzeitg festgelegten Prefix zurück
     * @return der derzeitge Prefix
     */
    public String getPrefix(){
        if(this.activatedModule == null)
            return "§f[§eServer§f] §7";
        else
            return this.activatedModule.getModuleConfiguration().getModulePrefix() + " §7";
    }

    /**
     * Gibt den derzeitg festgelegte Farbe zurück
     * @return der derzeitge Farbe
     */
    public String getColor(){
        if(this.activatedModule == null)
            return "§f";
        else
            return this.activatedModule.getModuleConfiguration().getModuleColor();
    }

    /**
     * Setzt dem angegeben {@link Player} einen
     * Header und einen Footer
     *
     * @param player - das {@link Player} Objekt des Spielers
     */
    public void setModuleTablist(final Player player){
        if(this.defaultFooter == null)
            this.defaultFooter = new TextComponent("§7GitLab-Repository§8: §e/gitlab\n§7Java Documentation§8: §e/javadoc");

        User user = UserService.getInstance().getUser(player.getUniqueId());
        Language language = user.getPlayerLanguage();
        if(this.activatedModule == null){
            TextComponent header = new TextComponent("§eGommeHD.net §cApplication Server\n" + language.getTranslation("tab-no-module-loaded"));
            player.setPlayerListHeaderFooter(header, this.defaultFooter);
        } else {
            TextComponent header = new TextComponent("§eGommeHD.net §cApplication Server\n" + language.getTranslation("tab-module-loaded", getColor() + getActivatedModule().getModuleName(), (this.getActivatedModule().getGame().getMap() == null ? "§c" + language.getTranslation("tab-no-map-loaded") : this.getActivatedModule().getGame().getMap().getMapName())));
            player.setPlayerListHeaderFooter(header,  this.defaultFooter);
        }

    }

    /**
     * Aktualisiert die MOTD des Servers
     */
    public void updateMotd(){
        if(this.activatedModule == null){
            MinecraftServer.getServer().setMotd("§eGommeHD.net §cDeveloper Application Server\n" +
                    "§7Derzeit wird kein Modul gespielt");
        } else {
            MinecraftServer.getServer().setMotd("§eGommeHD.net §cDeveloper Application Server\n" +
                    "§7Aktives Modul: " + getColor() + getActivatedModule().getModuleName() + " §7auf der Map §e" + getActivatedModule().getGame().getMap().getMapName());
        }
    }

    /**
     * Gibt eine Map mit einem String-Key und dem dazugehörigen {@link Module} zurück
     * @return eine Map mit einem String-Key und dem dazugehörigen {@link Module}
     */
    public Map<String, Module> getModules() {
        return modules;
    }

    /**
     * @return Das derzeitig aktive {@link Module}
     */
    public Module getActivatedModule() {
        return activatedModule;
    }
}
