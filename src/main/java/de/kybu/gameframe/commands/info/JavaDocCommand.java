package de.kybu.gameframe.commands.info;

import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /javadoc Befehls
 * </p>
 * @author Felix Clay (kybuu)
 */
@de.kybu.gameframe.commands.Command(name = "gitlab")
public class JavaDocCommand extends Command {

    public JavaDocCommand() {
        super("javadoc");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        commandSender.sendMessage(ModuleService.getInstance().getPrefix() + "§7Java Documentation: §ehttps://kybu.dev/docs/GameFrame/");
        return false;
    }
}
