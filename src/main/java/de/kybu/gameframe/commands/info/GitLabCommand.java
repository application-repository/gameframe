package de.kybu.gameframe.commands.info;

import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /gitlab Befehls
 * </p>
 * @author Felix Clay (kybuu)
 */
@de.kybu.gameframe.commands.Command(name = "gitlab")
public class GitLabCommand extends Command {

    public GitLabCommand() {
        super("gitlab");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        commandSender.sendMessage(ModuleService.getInstance().getPrefix() + "§7GitLab-Repository: §ehttps://gitlab.com/application-repository");
        return false;
    }
}
