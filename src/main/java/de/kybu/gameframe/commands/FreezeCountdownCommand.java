package de.kybu.gameframe.commands;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.modules.misc.Module;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /freezecountdown Befehls
 * </p>
 * @author Felix Clay (kybuu)
 */
@de.kybu.gameframe.commands.Command(name = "freezecountdown")
public class FreezeCountdownCommand extends Command {

    protected FreezeCountdownCommand() {
        super("freezecountdown");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {

        final Player player = (Player) commandSender;
        final User user = UserService.getInstance().getUser(player.getUniqueId());
        this.executeFreeze(player, user);

        return false;
    }

    private void executeFreeze(final Player player, final User user){
        if (/*!player.hasPermission("gameframe.freezecountdown")*/ false){
            user.sendTranslatedMessage("no-perms");
            return;
        }

        Module module = ModuleService.getInstance().getActivatedModule();
        if(module == null){
            user.sendTranslatedMessage("no-module-loaded", ModuleService.getInstance().getPrefix());
            return;
        }

        if(module.getGame().getCurrentCountdown() == null){
            user.sendTranslatedMessage("no-active-countdown", ModuleService.getInstance().getPrefix());
            return;
        }

        if(module.getGame().getCurrentCountdown().freeze()){
            BukkitUtil.broadcastTranslatedMessage("current-countdown-freezed", ModuleService.getInstance().getPrefix());
        } else {
            BukkitUtil.broadcastTranslatedMessage("current-countdown-unfreezed", ModuleService.getInstance().getPrefix());
        }
    }
}
