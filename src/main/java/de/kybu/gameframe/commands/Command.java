package de.kybu.gameframe.commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation und Command Klassen zu identifizieren und sie anschließend
 * mit der Hilfe dem Namen von {@link #name()} zu registrieren
 * @author Felix Clay (kybuu)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Command {

    String name();

}
