package de.kybu.gameframe.commands;

import de.kybu.gameframe.util.BukkitSerialization;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /kit Befehls
 * </p>
 * @author Felix Clay (kybuu)
 */
@de.kybu.gameframe.commands.Command(name = "kit")
public class KitCommand extends Command {

    protected KitCommand() {
        super("kit");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        Player player = (Player) commandSender;

        /*if(!player.hasPermission("gameframe.kit"))
            return false;

        if(ModuleService.getInstance().getActivatedModule() == null){
            player.sendMessage(ModuleService.getInstance().getPrefix() + "Es ist kein Modul geladen.");
            return false;
        }

        if(args.length < 2){
            player.sendMessage(ModuleService.getInstance().getPrefix() + "/kit <save, delete, load> (Name)");
            return false;
        }

        String kitName = args[1];

        if(args[0].equalsIgnoreCase("save")){

            String inventory64 = BukkitSerialization.itemStackArrayToBase64(player.getInventory().getContents());
            String armor64 = BukkitSerialization.itemStackArrayToBase64(player.getInventory().getArmorContents());

            Kit kit = new Kit(kitName, inventory64, armor64);

            if(KitService.getInstance().kitExists(kitName))
                KitService.getInstance().overwriteKit(kit);
            else
                KitService.getInstance().saveKit(kit);

            player.sendMessage(ModuleService.getInstance().getPrefix() + "Kit erfolgreich gespeichert!");

        } else if(args[0].equalsIgnoreCase("delete")){

            if(KitService.getInstance().kitExists(kitName)){
                player.sendMessage(ModuleService.getInstance().getPrefix() + "Es gibt kein Kit mit dem Namen &e" + kitName);
                return false;
            }

            KitService.getInstance().deleteKit(kitName);

        } else if(args[0].equalsIgnoreCase("load")) {

            if(!KitService.getInstance().kitExists(kitName)){
                player.sendMessage(ModuleService.getInstance().getPrefix() + "Es gibt kein Kit mit dem Namen &e" + kitName);
                return false;
            }

            Kit kit = KitService.getInstance().loadKit(kitName);
            player.getInventory().setContents(kit.getInventoryContents());
            player.getInventory().setArmorContents(kit.getArmorContents());
            player.sendMessage(ModuleService.getInstance().getPrefix() + "Kit erfolgreich geladen.");

        } else {
            player.sendMessage(ModuleService.getInstance().getPrefix() + "/kit <save, delete> (Name)");
        }*/

        return false;
    }
}
