package de.kybu.gameframe.commands;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.language.LanguageService;
import de.kybu.gameframe.util.message.ClickableMessage;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /language Befehls
 * </p>
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
@de.kybu.gameframe.commands.Command(name = "language")
public class LanguageCommand extends Command {

    private List<ClickableMessage> languages;

    public LanguageCommand() {
        super("language");
        this.languages = new ArrayList<>();
        this.generateMessages();
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        final Player player = (Player) commandSender;
        final User user = UserService.getInstance().getUser(player.getUniqueId());

        if(!user.canExecute()){
            user.sendTranslatedMessage("rate-limit-exceeded");
            return false;
        }

        executeLanguage(player, user, args);

        return false;
    }

    private void executeLanguage(final Player player, final User user, final String[] args){
        if(args.length == 0){
            user.sendTranslatedMessage("following-languages-available", ModuleService.getInstance().getPrefix());
            for (ClickableMessage message : this.languages) {
                user.sendMessage(message);
            }
            return;
        }

        if(!LanguageService.getInstance().getLanguages().containsKey(args[0])){
            user.sendTranslatedMessage("no-valid-input", "Language Key");
            return;
        }

        LanguageService.getInstance().updateLanguage(player, args[0]);
        user.updateLanguage(player);
        user.sendTranslatedMessage("language-successfully-updated", ModuleService.getInstance().getPrefix());

    }

    private void generateMessages(){
        LanguageService.getInstance().getLanguages().forEach((s, language) -> {
            ClickableMessage clickableMessage = new ClickableMessage(ModuleService.getInstance().getPrefix() + " §8- §e" + s);
            clickableMessage.addClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/language " + s));
            clickableMessage.addHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§f/language " + s).create()));
            this.languages.add(clickableMessage);
        });
    }
}
