package de.kybu.gameframe.commands;

import de.kybu.gameframe.game.Game;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.modules.misc.Module;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /start Befehls
 * </p>
 * @author Felix Clay (kybuu)
 */
@de.kybu.gameframe.commands.Command(name = "start")
public class StartCommand extends Command {

    protected StartCommand() {
        super("start");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        final Player player = (Player) commandSender;
        final User user = UserService.getInstance().getUser(player.getUniqueId());

        if (/*!player.hasPermission("gameframe.start")*/ false){
            user.sendTranslatedMessage("no-perms");
            return false;
        }

        Module module = ModuleService.getInstance().getActivatedModule();

        if(module == null){
            user.sendTranslatedMessage("no-module-loaded", ModuleService.getInstance().getPrefix());
            return false;
        }

        if(!module.getGame().isTurnedBased()){
            user.sendTranslatedMessage("start-not-available", ModuleService.getInstance().getPrefix());
            return false;
        }

        Game game = module.getGame();
        if(game.getCurrentCountdown() == null){
            user.sendTranslatedMessage("start-not-available", ModuleService.getInstance().getPrefix());
            return false;
        }

        if(game.getCurrentStatus() != GameStatus.LOBBY || game.getCurrentCountdown().getCurrentSeconds() <= 5){
            user.sendTranslatedMessage("start-not-available", ModuleService.getInstance().getPrefix());
            return false;
        }

        game.getCurrentCountdown().setCurrentSeconds(5);
        return false;
    }
}
