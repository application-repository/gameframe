package de.kybu.gameframe.commands;

import de.kybu.gameframe.util.language.Language;
import de.kybu.gameframe.util.language.LanguageService;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /reloadlang Befehls
 * </p>
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
@de.kybu.gameframe.commands.Command(name = "reloadlang")
public class ReloadLangCommand extends Command {

    protected ReloadLangCommand() {
        super("reloadlang");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        final Player player = (Player) commandSender;
        final User user = UserService.getInstance().getUser(player.getUniqueId());
        executeUpdate(player, user, args);
        return false;
    }

    public void executeUpdate(final Player player, final User user, final String[] args){

        if(!player.hasPermission("gameframe.reloadlang")){
            user.sendTranslatedMessage("no-perms");
            return;
        }

        if(!user.canExecute()){
            user.sendTranslatedMessage("rate-limit-exceeded");
            return;
        }

        if(args.length == 0){
            user.sendTranslatedMessage("wrong-command-usage", "/reloadlang (Key)");
            return;
        }

        Language language = LanguageService.getInstance().getLanguages().get(args[0]);
        if(language == null){
            user.sendTranslatedMessage("no-valid-input", "Key");
            return;
        }

        try {
            language.reload();
            user.sendTranslatedMessage("operation-executed-successfully");
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
}
