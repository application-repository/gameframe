package de.kybu.gameframe.commands;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;

import java.lang.reflect.Type;

/**
 * Utility Klasse
 * <p>
 *     Diese Klasse verinfacht mit Hilfe der {@link #registerCommand(Type)} Methode
 *     das Registrieren von Befehlen
 * </p>
 * @author Felix Clay (kybu)
 */
public class CommandLoader {

    /**
     * Diese Method registriert den Befehl für den {@link Type} Parameter.
     *
     * @param type, der Typ der Command Klasse
     * @throws Throwable wenn der Type keine gültige Klasse ist
     */
    public static void registerCommand(Type type) throws Throwable {

        Class<?> clazz = Class.forName(type.getTypeName());
        if(!clazz.isAnnotationPresent(Command.class))
            return;

        Command command = clazz.getAnnotation(Command.class);
        ((CraftServer) Bukkit.getServer()).getCommandMap().register(command.name(), (org.bukkit.command.Command) clazz.newInstance());

    }

}
