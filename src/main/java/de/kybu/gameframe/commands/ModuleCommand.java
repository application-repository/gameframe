package de.kybu.gameframe.commands;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.game.inventory.inventories.ModuleInventory;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command Klasse
 * <p>
 *     Dies Klasse beinhaltet die gesamte Logik des
 *     /module Befehls
 * </p>
 * @author Felix Clay (kybuu)
 */
@de.kybu.gameframe.commands.Command(name = "module")
public class ModuleCommand extends Command {

    protected ModuleCommand() {
        super("module");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        final Player player = (Player) commandSender;
        final User user = UserService.getInstance().getUser(player.getUniqueId());

        if (/*!player.hasPermission("gameframe.module")*/ false){
            user.sendTranslatedMessage("no-perms");
            return false;
        }

        if (args.length == 0) {
            this.executeModuleDefault(user);
            return false;
        }

        if (args[0].equalsIgnoreCase("load")) {
            this.executeModuleLoad(player, args, user);
        } else if (args[0].equalsIgnoreCase("drop")) {
            this.executeModuleDrop();
        } else if(args[0].equalsIgnoreCase("info")){
            this.executeModuleInfo(player);
        } else {
            this.executeModuleList(player, user);
        }

        return false;
    }

    private void executeModuleDefault(final User user){
        user.sendTranslatedMessage("wrong-command-usage", "/module <list, drop, info, load (Name)>");
    }

    private void executeModuleList(final Player player, final User user){
        user.sendTranslatedMessage("modules-loading", ModuleService.getInstance().getPrefix());

        GameFrame.getInstance().getExecutorService().execute(() -> {
            GameInventory gameInventory = GameInventory.INVENTORIES.get("modules_list");
            if(gameInventory == null)
                gameInventory = new ModuleInventory();

            player.openInventory(gameInventory.getInventory());
        });
    }

    private void executeModuleLoad(final Player player, final String[] args, final User user){
        if(ModuleService.getInstance().getActivatedModule() != null){
            user.sendTranslatedMessage("already-active-module");
            return;
        }

        if (args.length < 2) {
            user.sendTranslatedMessage("wrong-command-usage", "/module load (Name)");
            return;
        }

        if (!ModuleService.getInstance().getModules().containsKey(args[1])) {
            user.sendTranslatedMessage("module-not-found", ModuleService.getInstance().getPrefix());
            return ;
        }

        if(player.getOpenInventory() != null)
            player.closeInventory();

        ModuleService.getInstance().deactivate();
        ModuleService.getInstance().activateModule(args[1]);
    }

    private void executeModuleDrop(){
        ModuleService.getInstance().deactivate();
    }

    private void executeModuleInfo(final Player player){
        player.sendMessage("ram usage = " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1000000) + "mb");

    }
}
