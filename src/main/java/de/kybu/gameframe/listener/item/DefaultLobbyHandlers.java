package de.kybu.gameframe.listener.item;


import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Handler Klasse
 * <p>
 *     Diese Klasse handelt das
 *     {@link DefaultLobbyHandlers} von Bukkit
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 14.02.2021
 */
public class DefaultLobbyHandlers implements Listener {

    @EventHandler
    public void handle(final BlockBreakEvent event){
        if(ModuleService.getInstance().getActivatedModule() == null){
            event.setCancelled(true);
            return;
        }

        switch (ModuleService.getInstance().getActivatedModule().getGame().getCurrentStatus()){
            case LOBBY:
            case ENDING:
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(final BlockPlaceEvent event){
        if(ModuleService.getInstance().getActivatedModule() == null){
            event.setCancelled(true);
            return;
        }

        switch (ModuleService.getInstance().getActivatedModule().getGame().getCurrentStatus()){
            case LOBBY:
            case ENDING:
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(final FoodLevelChangeEvent event){
        if(ModuleService.getInstance().getActivatedModule() == null){
            event.setCancelled(true);
            return;
        }

        switch (ModuleService.getInstance().getActivatedModule().getGame().getCurrentStatus()){
            case LOBBY:
            case ENDING:
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(final EntityDamageEvent event){
        if(ModuleService.getInstance().getActivatedModule() == null){
            event.setCancelled(true);
            return;
        }

        switch (ModuleService.getInstance().getActivatedModule().getGame().getCurrentStatus()){
            case LOBBY:
            case ENDING:
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(final HangingBreakEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void handle(final PlayerDropItemEvent event){
        if(ModuleService.getInstance().getActivatedModule() == null){
            event.setCancelled(true);
            return;
        }

        switch (ModuleService.getInstance().getActivatedModule().getGame().getCurrentStatus()){
            case LOBBY:
            case ENDING:
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(final EntitySpawnEvent event){
        switch (event.getEntity().getType()){
            case ARROW:
            case PLAYER:
            case SHEEP:
            case FIREWORK:
            case VILLAGER:
            case ARMOR_STAND:
            case DROPPED_ITEM:
                event.setCancelled(false);
                break;
            default:
                event.setCancelled(true);
                break;
        }
    }

    @EventHandler
    public void handle(final PlayerInteractEvent event){
        if(ModuleService.getInstance().getActivatedModule() == null){
            event.setCancelled(true);
            return;
        }

        switch (ModuleService.getInstance().getActivatedModule().getGame().getCurrentStatus()){
            case LOBBY:
            case ENDING:
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(final PlayerArmorStandManipulateEvent event){
        event.setCancelled(true);
    }

}
