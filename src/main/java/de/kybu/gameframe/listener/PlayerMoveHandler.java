package de.kybu.gameframe.listener;

import de.kybu.gameframe.GameFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Handler Klasse
 * <p>
 *     Diese Klasse handelt das
 *     {@link PlayerMoveEvent} von Bukkit
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 07.02.2021
 */
public class PlayerMoveHandler implements Listener {

    @EventHandler
    public void handle(final PlayerMoveEvent event){
        final Player player = event.getPlayer();

        if(player.getWorld().getName().equals("world") && player.getLocation().getY() <= 0)
            player.teleport(GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation());
    }

}
