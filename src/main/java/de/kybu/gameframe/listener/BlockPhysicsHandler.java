package de.kybu.gameframe.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;

/**
 * Handler Klasse
 * <p>
 *     Diese Klasse handelt das
 *     {@link BlockPhysicsHandler} von Bukkit
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 11.02.2021
 */
public class BlockPhysicsHandler implements Listener {

    @EventHandler
    public void handle(final BlockPhysicsEvent event){
        event.setCancelled(true);
    }

}
