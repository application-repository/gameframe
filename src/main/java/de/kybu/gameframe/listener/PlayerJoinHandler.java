package de.kybu.gameframe.listener;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.map.Map;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.modules.misc.Module;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import javax.annotation.Nullable;

/**
 * Handler Klasse
 * <p>
 *     Diese Klasse handelt das
 *     {@link PlayerJoinEvent} von Bukkit
 * </p>
 *
 * @author Felix Clay (kybuu)
 */
public class PlayerJoinHandler implements Listener {

    @EventHandler
    public void handle(final PlayerJoinEvent event){
        event.setJoinMessage(null);

        UserService.getInstance().registerUser(event.getPlayer());
        ModuleService.getInstance().setModuleTablist(event.getPlayer());

        if(ModuleService.getInstance().getActivatedModule() != null) {
            Module module = ModuleService.getInstance().getActivatedModule();
            if(!module.getModuleConfiguration().isTurnBased()){

                Map map = module.getGame().getMap();
                event.getPlayer().teleport(map.getMapLocations().get("spawn").toLocation());

            }
        } else {
            event.getPlayer().teleport(GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation());
            event.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
            BukkitUtil.resetPlayer(event.getPlayer(), true, true);
            BukkitUtil.clearInventory(event.getPlayer());
            BukkitUtil.showAllHiddenPlayers(event.getPlayer());
        }

        IAchievementPlayerProvider playerProvider = IAchievementPlayerProvider.getInstance();
        Futures.addCallback(playerProvider.getAchievementPlayer(event.getPlayer().getUniqueId()), new FutureCallback<IAchievementPlayer>() {
            @Override
            public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                if(!iAchievementPlayer.hasAchievement(0))
                    iAchievementPlayer.unlockAchievement(0 /* First Join Achievement */);
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        }, GameFrame.getInstance().getExecutorService());

    }

}
