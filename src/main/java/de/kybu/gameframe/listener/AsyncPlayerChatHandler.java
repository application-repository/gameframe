package de.kybu.gameframe.listener;

import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Handler Klasse
 * <p>
 *     Diese Klasse handelt das
 *     {@link AsyncPlayerChatEvent} von Bukkit
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class AsyncPlayerChatHandler implements Listener {

    @EventHandler
    public void handle(final AsyncPlayerChatEvent event){
        final Player player = event.getPlayer();

        if(ModuleService.getInstance().getActivatedModule() == null){
            if(event.getMessage().contains("%"))
                event.setMessage(event.getMessage().replace("%", ""));

            event.setFormat("§c" + player.getName() + "§7: §f" + event.getMessage());
        }
    }

}
