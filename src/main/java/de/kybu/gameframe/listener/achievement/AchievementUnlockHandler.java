package de.kybu.gameframe.listener.achievement;

import de.kybu.achievements.common.event.AchievementUnlockedEvent;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.inventories.AchievementsInventory;
import de.kybu.gameframe.item.items.AchievementsItem;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.message.ClickableMessage;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Handler Klasse
 * <p>
 *     Diese Klasse handelt das
 *     {@link AchievementUnlockedEvent} aus meinem Achievement System
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class AchievementUnlockHandler implements Listener {

    @EventHandler
    public void handle(final AchievementUnlockedEvent event){
        final Player player = Bukkit.getPlayer(event.getUuid());
        final User user = UserService.getInstance().getUser(player.getUniqueId());
        final String prefix = ModuleService.getInstance().getPrefix();

        ClickableMessage clickableMessage = new ClickableMessage(prefix + "§7§kHahaLolDuHastErfolgxDD" +
                "\n" + prefix + "§7" +
                "\n" + prefix + "§7Du hast den Erfolg §e\"" + event.getAchievement().getAchievementName() + "\" §7erzielt!" +
                "\n" + prefix + "§7" +
                "\n" + prefix + "§7§kHgwBroDuHastEsGeschafft");
        StringBuilder stringBuilder = new StringBuilder();
        for (String line : event.getAchievement().getAchievementDescription()) {
            stringBuilder.append("§7").append(line).append("\n");
        }

        if(stringBuilder.toString().endsWith("\n"))
            stringBuilder.setLength(stringBuilder.length() - 1);
        
        clickableMessage.addHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(stringBuilder.toString()).create()));
        user.sendMessage(clickableMessage);

        player.playSound(player.getLocation(), Sound.LEVEL_UP, 3, 2);

        GameFrame.getInstance().getExecutorService().execute(() -> {
            if(AchievementsItem.getInstance().getAchievementsInventoryMap().containsKey(player.getUniqueId()))
                AchievementsItem.getInstance().getAchievementsInventoryMap().get(player.getUniqueId()).updateInventory();
        });
    }

}
