package de.kybu.gameframe.listener;

import de.kybu.gameframe.util.player.UserService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Handler Klasse
 * <p>
 *     Diese Klasse handelt das
 *     {@link PlayerQuitEvent} von Bukkit
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class PlayerQuitHandler implements Listener {

    @EventHandler
    public void handle(final PlayerQuitEvent event){
        event.setQuitMessage(null);
        UserService.getInstance().dropUser(event.getPlayer().getUniqueId());
    }

}
