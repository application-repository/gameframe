package de.kybu.gameframe.game;

/**
 * Game Status
 * <p>
 *     Ein Enum, welches alle möglichen
 *     Game Sates eines Spieles beinhaltet
 * </p>
 *
 * @author Felix Clay (kybuu)
 */
public enum GameStatus {

    LOBBY,
    RUNNING,
    ENDING,
    FFA;

}
