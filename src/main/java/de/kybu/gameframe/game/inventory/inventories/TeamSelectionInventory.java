package de.kybu.gameframe.game.inventory.inventories;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.item.Item;
import de.kybu.gameframe.teams.Team;
import de.kybu.gameframe.teams.TeamService;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Team Selection Inventory
 * <p>
 *     Das Team Inventar, in
 *     welchem alle verfügbaren Teams
 *     gelistet werden
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class TeamSelectionInventory extends GameInventory {

    private static TeamSelectionInventory instance;

    /**
     * Konstruktor der {@link TeamSelectionInventory} Klasse
     */
    public TeamSelectionInventory() {
        super("§7Team Selection", 9, "team_selection");
        this.registerClickListener(GameFrame.getInstance());
    }

    /**
     * Füllt das Inventar mit den nötigen Items
     */
    public void fillUpInventory(){
        this.getInventory().clear();
        for (Team team : TeamService.getInstance().getRegisteredTeams().values()) {
            List<String> names = new ArrayList<>();
            team.getTeamMembers().forEach((uuid, teamPlayer) -> names.add(teamPlayer.getPlayer().getName()));

            List<String> lore = new ArrayList<>();
            lore.add("§a");
            for(int i = 0; i < team.getMaxPlayers(); i++){
                if((names.size()-1) >= i)
                    lore.add("§7» " + team.getTeamColor() + names.get(i));
                else
                    lore.add("§7» §8-");
            }
            lore.add("§f");
            lore.add("§7Klicke, um " + team.getTeamColor() + "Team " + team.getTeamName() + " §7beizutreten!");

            ItemStack itemStack = new Item(Material.LEATHER_BOOTS)
                    .setDisplayName(team.getTeamColor() + "Team " + team.getTeamName())
                    .addLoreAll(lore)
                    .setColor(team.getTeamArmorColor())
                    .build();

            this.getInventory().addItem(Item.addTag(itemStack, "Team", team.getTeamName()));
        }
    }

    @Override
    public void handle(InventoryClickEvent event) {
        event.setCancelled(true);

        final String teamName = Item.readTag(event.getCurrentItem(), "Team");
        final Player player = (Player) event.getWhoClicked();
        if(!"FAIL".equals(teamName)){
            
            Team team = TeamService.getInstance().findTeam(teamName);
            if(team == null){
                player.closeInventory();
                return;
            }

            if(TeamService.getInstance().assignPlayer(player, team))
                this.fillUpInventory();
            player.closeInventory();
        }
    }

    public static TeamSelectionInventory getInstance() {
        return (instance != null ? instance : (instance = new TeamSelectionInventory()));
    }
}
