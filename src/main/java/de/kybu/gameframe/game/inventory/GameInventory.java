package de.kybu.gameframe.game.inventory;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.item.Item;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Getter
public abstract class GameInventory implements Listener {

    public static final Map<Object, GameInventory> INVENTORIES = new HashMap<>();
    private static final Map<String, Listener> INVENTORY_LISTENERS = new HashMap<>();

    private volatile Inventory inventory;
    private final String displayName;
    private final int size;

    /**
     * Konstruktor der {@link GameInventory} Klasse
     *
     * @param displayName - der Titel des Inventares
     * @param size - die Größe des Inventares
     * @param saveKey - der Save Key des Inventares (null, wenn es nicht gecached werden soll)
     */
    public GameInventory(final String displayName, final int size, Object saveKey){
        this.displayName = displayName;
        this.size = size;

        if(saveKey != null)
            INVENTORIES.put(saveKey, this);

        this.inventory = createInventory();
    }

    /**
     * Erstellt das Inventory
     * @return das erstellte Inventar
     */
    private Inventory createInventory(){
        return Bukkit.createInventory(null, this.size, this.displayName);
    }

    /**
     * Setzt die Inventory Contents
     * @param contents - die Contents
     */
    public void setContents(final ItemStack[] contents){
        this.inventory.setContents(contents);
    }

    /**
     * Methode, welche aufgerufen wird sobald
     * ein Spieler in dieses Inventar klickt
     *
     * @param event - das {@link InventoryClickEvent}
     */
    public abstract void handle(final InventoryClickEvent event);

    /**
     * Regstriert das Klick Event
     *
     * @param javaPlugin - das zugehörige Plugin
     */
    public void registerClickListener(final JavaPlugin javaPlugin){
        if(INVENTORIES.containsKey(this.displayName))
            return;

        Bukkit.getPluginManager().registerEvents(this, javaPlugin);
        INVENTORIES.put(this.displayName, this);
    }

    /**
     * Das {@link InventoryClickEvent} des Inventares
     * @param event - Bukkit Event
     */
    @EventHandler
    public void onClick(final InventoryClickEvent event){
        if(event.getCurrentItem() == null)
            return;

        if(event.getCurrentItem().getItemMeta() == null)
            return;

        if(event.getInventory().getTitle().equals(this.displayName))
            handle(event);
    }

    /**
     * Setzt Glas Panels in einer Reihe
     * @param row - die Inventar Reihe
     */
    public void setHolder(int row){
        ItemStack holder = new Item(Material.STAINED_GLASS_PANE).setData((short) 15).setNoName().build();
        for(int i = (row * 9); i < ((row * 9) + 9); i++){
            this.inventory.setItem(i, holder);
        }
    }
}
