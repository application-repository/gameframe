package de.kybu.gameframe.game.inventory.inventories;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.modules.misc.Module;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Module Inventory
 * <p>
 *     Das Modul Inventar, in welchem
 *     alle verfügbaren Module
 *     gelistet werden.
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class ModuleInventory extends GameInventory {

    /**
     * Konstruktor der {@link ModuleInventory} Klasse
     */
    public ModuleInventory() {
        super("§eModules", 27, "module_list");
        this.registerClickListener(GameFrame.getInstance());
        this.setDefaults();
    }

    /**
     * Füllt das Inventar mit Items
     */
    private void setDefaults(){
        this.getInventory().clear();
        this.setHolder(0);
        this.setHolder(2);

        for(Module module : ModuleService.getInstance().getModules().values()){
            if(module.getItemStack() != null){
                this.getInventory().addItem(module.getItemStack());
            }
        }
    }

    @Override
    public void handle(InventoryClickEvent event) {
        if(event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE){
            event.setCancelled(true);
            return;
        }

        String moduleName = event.getCurrentItem().getItemMeta().getDisplayName().replace("§8» §e", "");
        ((Player)event.getWhoClicked()).chat("/module load " + moduleName);
        event.setCancelled(true);
    }
}
