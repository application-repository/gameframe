package de.kybu.gameframe.game.inventory.inventories;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementCache;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.item.Item;
import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 14.02.2021
 */
public class AchievementsInventory extends GameInventory {

    /**
     * Konstruktor der {@link AchievementsInventory} Klasse
     */

    private UUID uuid;

    public AchievementsInventory(final UUID uuid) {
        super("§8Achievements", 27, null);
        this.uuid = uuid;
        this.registerClickListener(GameFrame.getInstance());
        updateInventory();
    }

    public void updateInventory(){
        final String currentType = ModuleService.getInstance().getActivatedModule() != null
                ? ModuleService.getInstance().getActivatedModule().getModuleName()
                : "GameFrame";

        Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(this.uuid), new FutureCallback<IAchievementPlayer>() {
            @Override
            public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                IAchievementCache.getInstance().getAchievements().values()
                        .stream()
                        .filter(achievement -> achievement.getAchievementModule().equalsIgnoreCase(currentType))
                        .forEach(achievement -> {
                            ItemStack itemStack;

                            if(iAchievementPlayer.hasAchievement(achievement.getAchievementId())){
                                itemStack = new Item(Material.INK_SACK)
                                        .setDisplayName("§a" + achievement.getAchievementName())
                                        .setData((short) 10)
                                        .addLoreAll(toLoreList(achievement.getAchievementDescription()))
                                        .addLoreLine("§7")
                                        .addLoreLine("§aErledigt!")
                                        .build();
                            } else {
                                itemStack = new Item(Material.INK_SACK)
                                        .setDisplayName("§c" + achievement.getAchievementName())
                                        .setData((short) 8)
                                        .addLoreAll(toLoreList(achievement.getAchievementDescription()))
                                        .addLoreLine("§7")
                                        .addLoreLine("§cNicht erledigt!")
                                        .build();
                            }

                            getInventory().addItem(itemStack);
                        });
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        }, GameFrame.getInstance().getExecutorService());
    }

    @Override
    public void handle(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    public UUID getUuid() {
        return uuid;
    }

    private List<String> toLoreList(final String[] array){
        List<String> list = new ArrayList<>();
        for (String s : array) {
            list.add("§7" + s);
        }
        return list;
    }
}
