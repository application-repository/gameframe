package de.kybu.gameframe.game.inventory.inventories;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.item.Item;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

/**
 * Spectator Inventory
 * <p>
 *     Das Spectator Inventar,
 *     in welchem alle verfügbaren
 *     Spieler gelistet werden
 * </p>
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class SpectatorInventory extends GameInventory {

    private static SpectatorInventory instance;

    /**
     * Konstruktor der {@link SpectatorInventory} Klasse
     */
    public SpectatorInventory() {
        super("§8Wähle einen Spieler", 18, null);
        this.registerClickListener(GameFrame.getInstance());
    }

    /**
     * Aktualisiert das Inventar und setzt alle Items
     * @param players - die verfügbaren Spieler
     */
    public void updateInventory(Collection<Player> players){
        new Thread(() -> {
            synchronized (this.getInventory()){
                this.getInventory().clear();

                for(Player player : players){
                    ItemStack playerHead = new Item(Material.SKULL_ITEM)
                            .setSkullOwner(player.getName())
                            .setDisplayName(/*PLAYER RANK-COLOR CHECK*/"§c" + player.getName())
                            .build();
                    this.getInventory().addItem(Item.addTag(playerHead, "player", player.getName()));
                }

                for (HumanEntity entity : getInventory().getViewers()) {
                    ((Player) entity).updateInventory();
                }
            }
        }).start();
    }

    @Override
    public void handle(InventoryClickEvent event) {
        event.setCancelled(true);

        if(event.getCurrentItem().getType() != Material.SKULL_ITEM)
            return;

        final User user = UserService.getInstance().getUser(event.getWhoClicked().getUniqueId());

        String assignedPlayer = Item.readTag(event.getCurrentItem(), "player");
        if(!"FAIL".equals(assignedPlayer)){

            Player player = Bukkit.getPlayer(assignedPlayer);
            if(player == null){
                user.sendTranslatedMessage("player-not-playing-anymore", ModuleService.getInstance().getPrefix());
                event.getWhoClicked().closeInventory();
                return;
            }

            event.getWhoClicked().teleport(player.getLocation());
        }

    }

    /**
     * Öffnet einem Spieler das Inventory
     * @param player - das {@link Player} Objekt
     */
    public void openToPlayer(final Player player){
        player.openInventory(this.getInventory());
    }

    public static SpectatorInventory getInstance() {
        return (instance != null ? instance : (instance = new SpectatorInventory()));
    }

}
