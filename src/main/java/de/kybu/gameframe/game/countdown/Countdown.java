package de.kybu.gameframe.game.countdown;

import de.kybu.gameframe.GameFrame;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Abstrakte Countdown Klasse
 * <p>
 *     Die Countdown Klasse ist eine
 *     simple API Klasse, um das Erstellen
 *     von Countdowns um ein vielfaches zu vereinfachen
 * </p>
 *
 * @author Felix Clay (kybuu)
 */

@Getter
@Setter
public abstract class Countdown {

    private Timer timer;
    private int currentSeconds;
    private boolean freeze;

    public Countdown(int startSeconds){
        this.timer = new Timer();
        this.currentSeconds = startSeconds;
        this.freeze = false;
    }

    /**
     * Methode, sobald der Countdown 0 erreicht
     */
    public abstract void onFinish();

    /**
     * Methode, welche aufgerufen wird, solange
     *der Countdown noch nicht 0 erreicht hat
     */
    public abstract void onTick();

    /**
     * Startet einen Countdown
     */
    public void start(){
        this.timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(currentSeconds <= 0) {
                    Bukkit.getScheduler().runTask(GameFrame.getInstance(), () -> {
                       onFinish();
                    });
                    timer.cancel();
                    timer.purge();
                } else {
                    onTick();
                }

                if(!freeze)
                    currentSeconds--;
            }
        }, 1000, 1000);
    }

    /**
     * Stoppt einen Countdown
     */
    public void stop(){
        if(this.timer == null)
            return;

        this.timer.cancel();
        this.timer = null;
    }

    /**
     * Friert den Countdown ein
     * @return - ob der Countdown eingefroren wurde, ob weiterläuft
     */
    public boolean freeze(){
        return this.freeze = !freeze;
    }
}
