package de.kybu.gameframe.game.spawner;

import de.kybu.gameframe.GameFrame;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/**
 * Spawner
 * <p>
 *     Diese Klasse ist sowie die {@link de.kybu.gameframe.game.countdown.Countdown} Klasse
 *     eine API Klasse. Diese soll das erstellen von Item Spawnern
 *     vereinfachen.
 * </p>
 *
 * @author Felix Clay (kybuu)
 */

@Getter
@Setter
@AllArgsConstructor
public class Spawner {

    public static final Map<Material, List<Spawner>> SPAWNERS = new HashMap<>();
    private static final Map<Material, Timer> TIMER = new HashMap<>();
    private static final Random RANDOM = new Random();

    private final Material material;
    private final Location location;

    /**
     * Erstellt den Spawner
     */
    public void createSpawner(){
        if(!TIMER.containsKey(this.material))
            TIMER.put(this.material, new Timer());

        if(!SPAWNERS.containsKey(this.material))
            SPAWNERS.put(this.material, new ArrayList<>());

        SPAWNERS.get(this.material).add(this);
    }

    /**
     * Droppt ein Item des gesetzten Typen
     * an der gesetzten Location
     */
    public void dropItem(){
        ItemStack itemStack = new ItemStack(this.material, 1);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("" + RANDOM.nextInt() + "#");
        itemStack.setItemMeta(itemMeta);

        location.getWorld().dropItemNaturally(location, itemStack);
    }

    /**
     * Startet alle Spawner welches das angegebene Material haben
     *
     * @param material - das Material
     * @param seconds - in welchem Takt der Spawner droppen soll
     */
    public static void startSpawner(Material material, int seconds){
        if(TIMER.containsKey(material)){
            TIMER.get(material).scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    Bukkit.getScheduler().runTask(GameFrame.getInstance(), () -> {
                        SPAWNERS.get(material).forEach(Spawner::dropItem);
                    });
                }
            }, seconds * 1000L, seconds * 1000L);
        }
    }

    /**
     * Stoppt alle Spawner welches das angegebene Material habe
     *
     * @param material - das Material
     */
    public static void delete(Material material){
        SPAWNERS.remove(material);
        TIMER.get(material).cancel();
        TIMER.get(material).purge();
        TIMER.remove(material);
    }

    /**
     * Stoppt alle Spawner
     */
    public static void deleteAll(){
        SPAWNERS.clear();
        TIMER.forEach((material1, timer) -> {
            timer.cancel();
            timer.purge();
        });
        TIMER.clear();
    }
}
