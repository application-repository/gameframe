package de.kybu.gameframe.game;

import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.map.Map;
import de.kybu.gameframe.map.MapService;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.modules.misc.GameSize;
import de.kybu.gameframe.teams.TeamService;
import lombok.Getter;
import lombok.Setter;

/**
 * Abstrakte Game Klasse
 * <p>
 *     Diese Klasse ist das Herzstück eines Spielmodus.
 *     Es beinhaltet zumindest die derzeitige Größe des Spieles,
 *     die geladene Map, den derzeitig laufenden {@link Countdown}
 *     sowie den derzeitigen Status des Spieles
 * </p>
 *
 * @author Felix Clay (kybuu)
 */

@Getter
@Setter
public abstract class Game {

    private GameStatus currentStatus;
    private boolean isTurnedBased;
    private GameSize gameSize;

    private Map map;

    private Countdown currentCountdown;

    /**
     * Konstruktor der {@link Game} Klasse
     * @param isTurnBased - Ob das Spiel Runden basiert ist oder nicht
     */
    public Game(boolean isTurnBased){
        this.isTurnedBased = isTurnBased;
        this.currentStatus = isTurnBased ? GameStatus.LOBBY : GameStatus.FFA;
    }

    /**
     * Startet die Vorbereitungsphase des Spieles
     */
    public abstract void startPrepare();

    /**
     * Startet das Spiel
     */
    public abstract void startGame();

    /**
     * Stopt das Spiel
     */
    public abstract void stopGame();

    /**
     * Setzt grundlegende Werte des Spieles zurück,
     * lädt die Map neu und registriert die Teams dafür neu
     */
    public abstract void resetGame();

    /**
     * Registriert alle nötigen Teams
     */
    public void registerTeams() {
        TeamService.getInstance().registerTeams(gameSize.getTeamCount(), gameSize.getPlayersPerTeam());
    }

    /**
     * Lädt eine zufällig Map
     */
    public void loadMap() {
        if(this.map != null){
            MapService.getInstance().unloadMap(this.map.getMapName());
            this.map = null;
        }

        String name = ModuleService.getInstance().getActivatedModule().getModuleName();
        MapService.getInstance().cacheMaps(name, gameSize.getTeamCount() + "x" + gameSize.getPlayersPerTeam());
        this.map = MapService.getInstance().loadMap(MapService.getInstance().getRandomMap());
    }
}
