package de.kybu.gameframe.teams;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Diese Klasse beinhaltet
 * alles noetigen Informationen ueber ein Team
 *
 * @author Felix Clay (kybuu)
 */
@Getter
@Setter
public class Team {

    private final String teamName;
    private final String teamColor;
    private final Color teamArmorColor;
    private final int maxPlayers;
    private final Map<UUID, TeamPlayer> teamMembers;
    private Object customTeamObject;

    private Location teamSpawnLocation;

    /**
     * Konstruktor der {@link Team} Klasse
     * @param teamName - der Name des Teams
     * @param maxPlayers - die maximalen Spieler des Teams
     * @param teamColor - die Farbe des Teams
     * @param color - das {@link Color} Objekt des Teams
     */
    public Team(String teamName, int maxPlayers, String teamColor, final Color color){
        this.teamName = teamName;
        this.maxPlayers = maxPlayers;
        this.teamColor = teamColor;
        this.teamArmorColor = color;
        this.teamMembers = new ConcurrentHashMap<>();
    }

    /**
     * @return Alle ueberlebenden Spieler eines Teams
     */
    public List<TeamPlayer> getAliveTeamMembers(){
        return this.teamMembers.values().stream()
                .filter(TeamPlayer::isAlive)
                .collect(Collectors.toList());
    }

    /**
     * Toetet einen lebenden Spieler
     * @param uuid - die {@link UUID} des Spielers
     */
    public void killTeamMember(final UUID uuid){
        this.teamMembers.get(uuid).setAlive(false);
    }

    /**
     * Entfernt einen Spieler aus dem Team
     * @param uuid - die {@link UUID} des Spielers
     */
    public void removePlayer(UUID uuid){
        this.teamMembers.remove(uuid);
    }

    /**
     * Fuegt einen Spieler dem Team hinzu
     * @param player - das {@link Player} Objekt des Spielers
     */
    public void addPlayer(Player player){
        this.teamMembers.put(player.getUniqueId(), new TeamPlayer(player, true));
    }
}
