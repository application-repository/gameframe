package de.kybu.gameframe.teams;

import de.kybu.gameframe.modules.ModuleService;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class TeamService {

    private static TeamService instance;

    private final Map<Integer, Team> registeredTeams;
    private final Map<UUID, Team> teamPlayers;

    public static TeamService getInstance() {
        return (instance != null ? instance : (instance = new TeamService()));
    }

    /**
     * Konstruktor der {@link TeamService} Klasse
     */
    public TeamService(){
        this.registeredTeams = new HashMap<>();
        this.teamPlayers = new ConcurrentHashMap<>();
    }

    /**
     * Registriert (teams) Teams, mit jeweils maximal (maxPlayers) Spielern
     * @param teams - die Anzahl der Teams
     * @param maxPlayers - maximalen Spieler pro Team
     */
    public void registerTeams(int teams, int maxPlayers){
        this.registeredTeams.clear();

        for(int i = 1; i < teams + 1; i++){
            this.registeredTeams.put(i, new Team(getTeamName(i), maxPlayers, getTeamColor(i), getArmorColor(i)));
        }
    }

    /**
     * Gibt einen Team Namen für die Team ID zurück
     * @param id - die Team ID
     * @return Team Name
     */
    private String getTeamName(int id){
        switch (id){
            case 1:
                return "Rot";
            case 2:
                return "Grün";
            case 3:
                return "Blau";
            case 4:
                return "Gelb";
        }
        return "Weiß";
    }

    /**
     * Gibt eine Team Farbe für die Team ID zurück
     * @param id - die Team ID
     * @return Team Farbe
     */
    private String getTeamColor(int id){
        switch (id){
            case 1:
                return "§c";
            case 2:
                return "§a";
            case 3:
                return "§b";
            case 4:
                return "§e";
        }
        return "§f";
    }

    /**
     * Gibt eine Team {@link Color} für die Team ID zurück
     * @param id - die Team ID
     * @return Team {@link Color}
     */
    private Color getArmorColor(final int id){
        switch (id){
            case 1:
                return Color.RED;
            case 2:
                return Color.LIME;
            case 3:
                return Color.AQUA;
            case 4:
                return Color.YELLOW;
        }
        return Color.BLACK;
    }

    /**
     * Fuegt alle Spieler, welche nicht Mitglied eines Teams sind,
     * zu einem zufällig ausgewählten Team hinzu, insofern
     * es nicht voll ist.
     */
    public void assignPlayersRandom(){
        Bukkit.getOnlinePlayers().forEach(player -> {
            if(!this.teamPlayers.containsKey(player.getUniqueId())){
                for(Team team : this.registeredTeams.values()){
                    if(team.getTeamMembers().size() < team.getMaxPlayers()){
                        team.addPlayer(player);
                        player.sendMessage(ModuleService.getInstance().getPrefix() + "§7Du wurdest " + team.getTeamColor() + "Team " + team.getTeamName() + " §7zugewiesen!");
                        this.teamPlayers.put(player.getUniqueId(), team);
                         break;
                    }
                }
            }
        });
    }

    /**
     * Fuegt den Spieler zu dem Team hinzu, wenn das Team voll sein sollte,
     * wird dem Spieler eine Fehlermeldung ausgegeben.
     *
     * @param player - das {@link Player} Objekt des Spielers
     * @param team - das {@link Team} Objekt, des ausgewaehlten Teams
     * @return ob der Spieler erfolgreich hinzugefuegt werden konnte
     */
    public boolean assignPlayer(Player player, Team team){
        if(team.getTeamMembers().size() >= team.getMaxPlayers()){
            player.playSound(player.getLocation(), Sound.ANVIL_LAND, 3, 1);
            player.sendMessage(ModuleService.getInstance().getPrefix() + "Das Team ist bereits voll.");
            return false;
        }

        if(this.teamPlayers.containsKey(player.getUniqueId()))
            this.teamPlayers.get(player.getUniqueId()).removePlayer(player.getUniqueId());

        this.teamPlayers.put(player.getUniqueId(), team);
        team.addPlayer(player);
        player.sendMessage(ModuleService.getInstance().getPrefix() + "§7Du wurdest " + team.getTeamColor() + "Team " + team.getTeamName() + " §7zugewiesen!");
        return true;
    }

    /**
     * Entfernt alle Teams sowie die Spieler darin
     */
    public void reset(){
        this.teamPlayers.clear();
        this.registeredTeams.clear();
    }

    /**
     * @return Alle registrierten Teams
     */
    public Map<Integer, Team> getRegisteredTeams() {
        return registeredTeams;
    }

    /**
     * @return Eine Map, welche als Key eine UUID hat, und als Value
     * das {@link Team} eines Spielers
     */
    public Map<UUID, Team> getTeamPlayers() {
        return teamPlayers;
    }

    /**
     * Findet ein Team mit dem Team Namen
     * @param teamName - der gesuchte Team Name
     * @return das {@link Team} Objekt, des gesuchten Teams
     */
    public Team findTeam(String teamName){
        for(Team team : this.registeredTeams.values()){
            if(team.getTeamName().equalsIgnoreCase(teamName))
                return team;
        }

        return null;
    }

    /**
     * Gibt das Team eines Spielers zurück
     * @param player - das {@link Player} Objekt eines Spielers
     * @return das {@link Team} des Spielers
     */
    public Team getPlayersTeam(final Player player){
       return this.teamPlayers.get(player.getUniqueId());
    }
}
