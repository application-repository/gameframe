package de.kybu.gameframe.teams;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

/**
 * Custom Player Objekt eines
 * Team Mitglieds
 *
 * @author Felix Clay (kybuu)
 */
@AllArgsConstructor
public class TeamPlayer {

    @Getter private final Player player;
    @Setter private boolean isAlive;

    public boolean isAlive() {
        return isAlive;
    }
}
