package de.kybu.gameframe.configurations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation
 * <p>
 *     Diese Annotation kennzeichnet Konfigurationsklassen,
 *     und gibt wenn nötig den statischen Pfad der Konfiguration mit.
 *
 *     (Nur für JSON Konfigurationen)
 * </p>
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Configuration {

    String path();

}
