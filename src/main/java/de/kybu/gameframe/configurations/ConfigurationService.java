package de.kybu.gameframe.configurations;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility Klasse
 * <p>
 *     Diese Klasse soll das Einlesen und Speichern von
 *     JSON Konfiguration um ein vielfaches vereinfachen.
 * </p>
 * @author Felix Clay (kybuu)
 */
public class ConfigurationService {

    private static Map<Type, Object> CONFIGURATIONS = new HashMap<>();

    private static ConfigurationService instance;
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Gibt eine Instanz der {@link ConfigurationService} zurück
     * @return Instanz der Klasse {@link ConfigurationService}
     */
    public static ConfigurationService getInstance() {
        return (instance != null ? instance : (instance = new ConfigurationService()));
    }

    /**
     * Methode um Konfigurationklassen einzulesen
     * <p>
     *     Mit dieser Methode kann man mit Hilfe des {@link Type} Parameters
     *     eine Konfigurationsklasse auslesen. Zu beachten hier ist,
     *     dass der {@link Type} die Annotation {@link Configuration} haben muss!
     *     Wenn nicht, gibt diese Method null zurück.
     * </p>
     * @param type, den Typ der Konfigurationklasse
     * @param <T>, der generische Typ der Output Klasse
     * @return Deserializierte Instanz der Konfiguration Klasse
     */
    public <T> T readConfiguration(Type type){
        try {
            Class<?> clazz = Class.forName(type.getTypeName());

            if(!clazz.isAnnotationPresent(Configuration.class))
                return null;

            Configuration configuration = clazz.getAnnotation(Configuration.class);

            File file = new File(configuration.path());
            if(!file.exists()){
                this.saveConfiguration(clazz.newInstance());
            }

            return GSON.fromJson(new String(Files.readAllBytes(file.toPath())), type);
        } catch (ClassNotFoundException | IOException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Methode um Konfigurationsklassen zu speichern
     * <p>
     *     Mit dieser Method kann man ganz einfach Konfigurationsklassen zu einer
     *     JSON-Konfiguration serializieren, auch hier ist es wichtig,
     *     dass der {@link Type} des T Parameters die Annotation
     *     {@link de.kybu.gameframe.commands.Command} hat.
     * </p>
     *
     * @param t, Klasse mit den gesetzen Werten
     * @param <T>
     */
    public <T> void saveConfiguration(T t){
        if(!t.getClass().isAnnotationPresent(Configuration.class))
            return;

        Configuration configuration = t.getClass().getAnnotation(Configuration.class);
        File file = new File(configuration.path());
        file = file.getParentFile();
        file.mkdirs();

        try(FileWriter writer = new FileWriter(configuration.path())){
            GSON.toJson(t, writer);
        } catch(IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Methode um Konfigurationklassen einzulesen
     * <p>
     *     Mit dieser Methode kann man mit Hilfe des {@link Type} Parameters
     *     eine Konfigurationsklasse auslesen.
     * </p>
     * @see #readConfiguration(Type) 
     * @param type, den Typ der Konfigurationklasse
     * @param path, Pfad welche ausgelesen werden soll
     * @param <T>
     * @return Deserializierte Instanz der Konfiguration Klasse
     */
    public <T> T readConfiguration(Type type, String path){
        try {
            Class<?> clazz = Class.forName(type.getTypeName());

            File file = new File(path);

            if(!file.exists()){
                this.saveConfiguration(clazz.newInstance(), path);
            }

            return GSON.fromJson(new String(Files.readAllBytes(file.toPath())), type);
        } catch (ClassNotFoundException | IOException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Methode um Konfigurationsklassen zu speichern
     * <p>
     *     Mit dieser Method kann man ganz einfach Konfigurationsklassen zu einer
     *     JSON-Konfiguration serializieren.
     * </p>
     * @param t, Klasse mit den gesetzen Werten
     * @param path, Pfad zu welchem gespeichert werden soll
     * @param <T>
     */
    public <T> void saveConfiguration(T t, String path){
        File file = new File(path);
        file.getParentFile().mkdirs();

        try(FileWriter writer = new FileWriter(file)){
            GSON.toJson(t, writer);
        } catch(IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Gibt eine Konfiguration aus dem Cache zurück
     * <p>
     *     Diese Method gibt eine Konfiguration aus dem Cache zurück,
     *     wenn diese noch nicht geladen wurde, wird diese
     *     mit der {@link #readConfiguration(Type)} Method geladen.
     * </p>
     * <p>
     *     Es können hiermit nur statische Konfigurationsdateien geladen werden
     *     (Konfigurationen, welche mit {@link Configuration} gekennzeichnet sind
     * </p>
     * @param type, Typ der Konfigurationsklasse
     * @return Konfigurationsobjekt
     */
    public static Object getConfiguration(Type type){
        return CONFIGURATIONS.getOrDefault(type, getInstance().readConfiguration(type));
    }
}
