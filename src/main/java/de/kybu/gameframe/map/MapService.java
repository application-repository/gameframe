package de.kybu.gameframe.map;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.configurations.ConfigurationService;
import de.kybu.gameframe.events.GameFrameMapLoadEvent;
import de.kybu.gameframe.events.GameFrameMapUnloadEvent;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.apache.commons.io.FileUtils;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 05.02.2021
 */
public class MapService {

    private static MapService instance;

    private List<String> maps;

    public static MapService getInstance() {
        return (instance != null ? instance : (instance = new MapService()));
    }

    /**
     * Lädt eine Map und setzt alle
     * noetigen Werte und GameRules
     * @param path - der Pfad der Map
     * @return das {@link Map} Objekt einer Map
     */
    public Map loadMap(String path){
        File file = new File(path);

        try {
            FileUtils.copyDirectoryToDirectory(file, Bukkit.getWorldContainer());
        } catch (IOException e) {
            e.printStackTrace();
        }

        WorldCreator worldCreator = new WorldCreator(file.getName());
        World world = Bukkit.createWorld(worldCreator);
        world.setDifficulty(Difficulty.EASY);
        world.getEntities().stream()
                .filter(entity -> entity.getType() != EntityType.ARMOR_STAND && entity.getType() != EntityType.PLAYER)
                .forEach(Entity::remove);

        world.setPVP(true);
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setGameRuleValue("randomTickSpeed", "0");
        world.setGameRuleValue("doFireTick", "false");
        world.setGameRuleValue("keepInventory", "false");

        Map map = new Map();
        try {
            map.init(new File(path + "/map.yml"));
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        Bukkit.getPluginManager().callEvent(new GameFrameMapLoadEvent(map));

        return map;
    }

    /**
     * Entlaedt eine Map
     * @param worldName - der Name der zu entladenen Map
     */
    public void unloadMap(String worldName){

        World world = Bukkit.getWorld(worldName);


        Location location = GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation();

        world.getPlayers().forEach(player -> {
            player.teleport(location);
        });

        Bukkit.unloadWorld(world, false);

        File file = new File(worldName);
        Bukkit.getScheduler().runTaskLater(GameFrame.getInstance(), () -> {
            if(file.exists()){
                try {
                    FileUtils.forceDelete(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 5);

        Bukkit.getPluginManager().callEvent(new GameFrameMapUnloadEvent(null));
    }

    /**
     * Laedt alle verfügbaren Maps fuer den
     * angegeben Typen in der angegebenen
     * Größe
     * @param type - der Game Typ
     * @param gameSize - die Game Größe
     */
    public void cacheMaps(String type, String gameSize){
        this.maps = new ArrayList<>();

        File file = new File("plugins/GameFrame/maps/" + type + "/" + gameSize);
        Arrays.stream(file.listFiles())
                .filter(files -> files.isDirectory())
                .forEach(files -> {
                    this.maps.add(files.getAbsolutePath());
                });
    }

    /**
     * Gibt eine zufällige {@link Map} zurück
     * @return eine zufällige {@link Map}
     */
    public String getRandomMap(){
        Random random = new Random();
        if(this.maps.size() == 1)
            return this.maps.get(0);
        else
            return this.maps.get(random.nextInt(this.maps.size() - 1));
    }
}
