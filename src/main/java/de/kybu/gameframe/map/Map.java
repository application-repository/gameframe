package de.kybu.gameframe.map;

import de.kybu.gameframe.util.XYZW;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.YamlConfig;

import java.util.HashMap;

/**
 * Konfigurations Klasse
 * <p>
 *     Diese Konfiguration beinahltet
 *     alle nötigen Informationen für eine Map.
 *     Ebenso befinden hier drinnen sich alle
 *     relevanten Locations der Map
 * </p>
 *
 * @author Felix Clay (kybuu)
 */
@Getter
@Setter
@AllArgsConstructor
public class Map extends YamlConfig {

    private String mapName;
    private transient String mapPath;
    private String mapBuilder;
    private int worldBorderMaximumSize;
    private int worldBorderMinimumSize;
    private java.util.Map<String, XYZW> mapLocations;

    public Map(){
        this.mapName = "";
        this.mapPath = "";
        this.mapBuilder = "";
        this.mapLocations = new HashMap<>();
        this.mapLocations.put("a", new XYZW("", 0, 0, 0,0, 0));
        this.worldBorderMaximumSize = 100;
        this.worldBorderMinimumSize = 50;
    }
}
