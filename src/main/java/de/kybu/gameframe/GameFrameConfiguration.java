package de.kybu.gameframe;

import de.kybu.gameframe.util.XYZW;
import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.YamlConfig;

import java.io.File;

/**
 * Konfigurations Klasse
 * <p>
 *     Diese Konfiguration beinhaltet ausschließlich die Location der Wartelobby.
 * </p>
 *
 * @author Felix Clay (kybuu)
 */
@Getter
@Setter
public class GameFrameConfiguration extends YamlConfig {

    private XYZW spawnLocation;

    public GameFrameConfiguration(){
        CONFIG_FILE = new File("plugins/GameFrame/config.yml");
        this.spawnLocation = new XYZW("", 0, 0, 0, 0, 0);
    }
}
