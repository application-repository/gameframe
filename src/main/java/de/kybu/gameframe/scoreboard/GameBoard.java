package de.kybu.gameframe.scoreboard;

import com.google.common.base.Preconditions;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * @author Dytanic
 */
public class GameBoard {

    private static final String PARAGRAPH = "§";

    private static final String OBJECTIVE_NAME = "dyn_scoreboard";

    private Scoreboard scoreboard;

    public GameBoard(final Scoreboard scoreboard) {
        this.scoreboard = Preconditions.checkNotNull(scoreboard, "Scoreboard is null");
    }

    public GameBoard apply(final Player player) {
        Preconditions.checkNotNull(player);
        player.setScoreboard(scoreboard);
        return this;
    }

    public GameBoard scoreboard(final Scoreboard scoreboard) {
        this.scoreboard = scoreboard;
        return this;
    }

    public Scoreboard scoreboard() {
        return scoreboard;
    }

    public GameBoard display() {

        final Objective objective = registerObjective();
        objective.setDisplayName("§f§lGOMMEHD.NET");

        return this;
    }

    public GameBoard set(final int index, String value) {
        return set(index, value, index);
    }

    public GameBoard set(final int index, String value, final int score) {

        Preconditions.checkNotNull(value, "value is null");
        Preconditions.checkNotNull(scoreboard, "scoreboard is null");

        Team team = getTeam(index);
        addTeamEntry(team, index);

        if (value.length() > 32) {
            value = value.substring(0, 32);
        }

        final String finalValueFirst;
        final String finalValueSecond;

        if (value.length() > 16) {
            finalValueFirst = value.substring(0, 16);
            finalValueSecond = value.substring(16);
        } else {
            finalValueFirst = value;
            finalValueSecond = null;
        }

        team.setPrefix(finalValueFirst);

        if (finalValueSecond != null) {
            team.setSuffix(ChatColor.getLastColors(finalValueFirst) + finalValueSecond);
        }

        registerObjective().getScore(getNameByIndex(index)).setScore(score);
        return this;
    }

    public GameBoard setNotOverwrite(final int index, String value, final int score, final int secondScore) {

        Preconditions.checkNotNull(value, "value is null");
        Preconditions.checkNotNull(scoreboard, "scoreboard is null");

        Team team = getTeam(index);
        addTeamEntry(team, index);

        if (value.length() > 32) {
            value = value.substring(0, 32);
        }

        final String finalValueFirst;
        final String finalValueSecond;

        if (value.length() > 16) {
            finalValueFirst = value.substring(0, 16);
            finalValueSecond = value.substring(16);
        } else {
            finalValueFirst = value;
            finalValueSecond = null;
        }

        team.setPrefix(finalValueFirst);

        if (finalValueSecond != null) {
            team.setSuffix(ChatColor.getLastColors(finalValueFirst) + finalValueSecond);
        }

        registerObjective().getScore(getNameByIndex(index)).setScore(secondScore);
        return this;
    }

    public GameBoard remove(int index) {

        Preconditions.checkNotNull(scoreboard, "scoreboard is null");
        scoreboard.resetScores(getNameByIndex(index));
        return this;
    }

    /*= ------------------------------------------------- =*/

    private Team getTeam(final int index) {

        Team team = scoreboard.getTeam(getNameByIndex(index));
        if (team == null) {
            team = scoreboard.registerNewTeam(getNameByIndex(index));
        }

        return team;
    }

    private void addTeamEntry(final Team team, final int index) {

        Preconditions.checkNotNull(team);

        if (!team.hasEntry(getNameByIndex(index))) {
            team.addEntry(getNameByIndex(index));
        }
    }

    private Objective registerObjective() {
        Objective objective = scoreboard.getObjective(OBJECTIVE_NAME);

        if (objective == null) {
            objective = scoreboard.registerNewObjective(OBJECTIVE_NAME, "dummy");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            objective.setDisplayName("§");
        }

        return objective;
    }

    private String getNameByIndex(final int index) {

        if (index < 10) {
            return PARAGRAPH + index;
        }

        switch (index) {
            case 10:
                return PARAGRAPH + "a";
            case 11:
                return PARAGRAPH + "b";
            case 12:
                return PARAGRAPH + "c";
            case 13:
                return PARAGRAPH + "d";
            case 14:
                return PARAGRAPH + "e";
            case 15:
                return PARAGRAPH + "f";
            default:
                return PARAGRAPH + "0";
        }
    }

}
